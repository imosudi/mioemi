<!DOCTYPE html>
<html>
  <head>
    <link type='text/css' rel='stylesheet' href='style.css'/>
    <title>Domain Availability</title>
  </head>
  <body>

<?
// prepare vars
$domain    = "google.com";     // domain to check
$r         = "taken";             // request type: domain availability
$apikey    = "demokey";        // your API key

// API call
$output = json_decode(file_get_contents(
    "http://api.whoapi.com/?domain=$domain&r=$r&apikey=$apikey"), true); 
if($output['status'] == 0){
    // show the result
    echo $output['taken'];
    }else{
    // show error
    echo $output['status_desc'];
}
?>

</body>
</html>
