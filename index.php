<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Public Profile of Mosudi Isiaka O.</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mosudi Isiaka, public profile of a season data center/network infrastructure engineer">
    <meta name="author" content="Mosudi Isiaka">    
    <!--link rel="shortcut icon" href="favicon.ico"-->

<link rel="apple-touch-icon" sizes="57x57" href="assets/images/favico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/images/favico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/images/favico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/images/favico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/images/favico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/images/favico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/images/favico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/images/favico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/images/favico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/images/favico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="assets/images/favico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favico/favicon-16x16.png">
<link rel="manifest" href="assets/images/favico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="assets/images/favico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'> 
    <!-- Global CSS -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">
    <!-- github acitivity css -->
    <link rel="stylesheet" href="assets/mioemi_script/octicons.min.css">
    <link rel="stylesheet" href="assets/mioemi_script/github-activity-0.1.0.min.css">
    
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/mioemi_script/html5shiv.js"></script>
      <script src="assets/mioemi_script/respond.min.js"></script>
    <![endif]-->
    

<!--Google Analytics for Mioemi.com-->	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91377637-1', 'auto');
  ga('send', 'pageview');

</script>
<!--Adsense-->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2567919409249142",
    enable_page_level_ads: true
  });
</script>
<!--Google Adsense-->
    
<script src='https://www.google.com/recaptcha/api.js'></script>

<style>
video { 
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    transform: translateX(-50%) translateY(-50%);
 /*background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;*/
  background-size: cover;
  transition: 1s opacity;
  -webkit-filter:brightness(0.8);
}
.stopfade { 
   opacity: .8;
}
</style>



	

<style type="text/css">
      .reveal-if-active {
  opacity: 0;
  max-height: 0;
  overflow: hidden;
  font-size: 16px;
  -webkit-transform: scale(0.8);
      -ms-transform: scale(0.8);
          transform: scale(0.8);
  -webkit-transition: 0.5s;
          transition: 0.5s;
}
.reveal-if-active label {
  display: block;
  margin: 0 0 3px 0;
}
.reveal-if-active input[type=text] {
  width: 100%;
}
input[type="radio"]:checked ~ .reveal-if-active, input[type="checkbox"]:checked ~ .reveal-if-active {
  opacity: 1;
  max-height: 100px;
  padding: 10px 20px;
  -webkit-transform: scale(1);
      -ms-transform: scale(1);
          transform: scale(1);
  overflow: visible;
}

form {
  max-width: 1200px;
  margin: 20px auto;
}
form > div {
  margin: 0 0 20px 0;
}
form > div label {
  font-weight: bold;
}
form > div > div {
  padding: 5px;
}
form > h4 {
  color: green;
  font-size: 24px;
  margin: 0 0 10px 0;
  border-bottom: 2px solid green;
}

</style>



<script src='https://www.google.com/recaptcha/api.js'></script>	
</head> 

<body>

<script src="assets/js/mind3.v3.min.js"></script>
<script>

var width = Math.max(960, innerWidth),
    height = Math.max(500, innerHeight);

var x1 = width / 2,
    y1 = height / 2,
    x0 = x1,
    y0 = y1,
    i = 0,
    r = 200,
    τ = 2 * Math.PI;

var canvas = d3.select("body").append("canvas")
    .attr("width", width)
    .attr("height", height)
    .on("ontouchstart" in document ? "touchmove" : "mousemove", move);

var context = canvas.node().getContext("2d");
context.globalCompositeOperation = "lighter";
context.lineWidth = 2;

d3.timer(function() {
  context.clearRect(0, 0, width, height);

  var z = d3.hsl(++i % 360, 1, .5).rgb(),
      c = "rgba(" + z.r + "," + z.g + "," + z.b + ",",
      x = x0 += (x1 - x0) * .1,
      y = y0 += (y1 - y0) * .1;

  d3.select({}).transition()
      .duration(2000)
      .ease(Math.sqrt)
      .tween("circle", function() {
        return function(t) {
          context.strokeStyle = c + (1 - t) + ")";
          context.beginPath();
          context.arc(x, y, r * t, 0, τ);
          context.stroke();
        };
      });
});

function move() {
  var mouse = d3.mouse(this);
  x1 = mouse[0];
  y1 = mouse[1];
  d3.event.preventDefault();
}

</script>

    <!-- ******HEADER****** --> 
    <header class="header">
        <div class="container">                       
            <img class="profile-image img-responsive pull-left" src="assets/images/profile.png" alt="Mosudi Isiaka" />
            <div class="profile-content pull-left">
                <h1 class="name">Mosudi Isiaka</h1>
                <h2 class="desc">Data Center Infrastructure</h2>  
<ul>
<ul class="list-unstyled">
<li><i class="fa fa-headphones"></i> <a href="#">Automation (Ansible)</a></li>
<li><i class="fa fa-headphones"></i> <a href="#">Virtualization and Cloud Computing</a></li>
<li><i class="fa fa-headphones"></i> <a href="#">Windows and Linux Servers Administration</a></li>
<li><i class="fa fa-headphones"></i> <a href="#">Network Architecture Design, Implementation and Administration</a></li>
<li><i class="fa fa-headphones"></i> <a href="#"> Routing, Link Bonding, Redundancy, Fail-over and Clustering</a></li>
<!--li><i class="fa fa-headphones"></i> <a href="#"> Backup and Distaster Recovery</a></li-->
<li><i class="fa fa-headphones"></i> <a href="#">VPN, Firewall & Intrusion Detection System</a></li>
<li><i class="fa fa-headphones"></i> <a href="#"> Storage Management (GlusterFS, SAN and NAS)</a></li>
 <li><i class="fa fa-headphones"></i> <a href="#"> Python Programming and Linux Shell/CGI Scripting</a></li>
<li><i class="fa fa-headphones"></i> <a href="#">Monitoring Systems (Icinga2)</a></li>
<!--li><i class="fa fa-headphones"></i> <a href="#"> Network Traffic Engineering</a></li-->
<li><i class="fa fa-headphones"></i> <a href="#"> Data Center Collocation On-boarding procedures</a></li>
</ul>
<p>
                <ul class="social list-inline">
                    <li><a href="https://github.com/imosudi"><i class="fa fa-github"></i></a></li>                   
                    <li><a href="https://plus.google.com/+MosudiIsiaka"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="http://ng.linkedin.com/pub/isiaka-mosudi/1b/7a2/936/"><i class="fa fa-linkedin"></i></a></li>
                    
            </div><!--//profile-->
            <a class="btn btn-cta-primary pull-right" href="http://mioemi.com/" ><i class="fa fa-paper-plane"></i> Contact Me</a>              
        </div><!--//container-->
    </header><!--//header-->
    
    <div class="container sections-wrapper">
        <div class="row">
            <div class="primary col-md-8 col-sm-12 col-xs-12">

                <section class="about section">
                    <div class="section-inner">
                        <h2 class="heading">About Me</h2>
                        <div class="content">
<p>
<div id="log"> </div>
<!-- <?php
  /**require_once('recaptchalib.php');
  $privatekey = "6LfTRwsUAAAAAPnOA3Pl7jP4kdvJYi3RnuoGe7yS";
  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
    die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
         "(reCAPTCHA said: " . $resp->error . ")");
  } else {
    // Your code here to handle a successful verification
  }**/
  ?>-->


<hr>
<h3>Current Positions:</h3>
<strong>Data Centre Manager </strong><br>
<a href="http://one.net.ng/">One Network Management Services Ltd.</a>  | Lagos , Nigeria<br>
Manager<br>
Feb. 2012 – May 2017 <br>
Responsibilities included install, configure, and support the LANs, WANs and Internet systems, Data Center
Collocation On-boarding, maintain network and workstation hardware and software, monitor network to ensure
network availability to all system users and perform necessary maintenance to support network availability, supervise
and train network support technicians and coordinate network security measures., manage capacity planning activities
to enhance the performance of the network resources, ...
<hr>

<strong>Technical Reviewer</strong><br>
<a href="https://www.packtpub.com/">Packt Publishing Limited</a> | Birmingham , United Kingdom<br>
Individual Contributor<br>
June 2015 – Jan. 2016 <br>
<ol>
<li> <a href="https://www.packtpub.com/application-development/mastering-python-high-performance">Mastering Python High Performance by Fernando Doglio</a>, Published Sept. 2015.</li>

<li><a href="https://www.packtpub.com/networking-and-servers/mastering-linux-network-administration"> Mastering Linux Network Administration by Jay Lacroix</a>, Published Nov. 2015</li>
</ol>
<hr>
</p>
                            <!--p>I  am  a  graduate  of  Electrical  and  Computer  Engineering  Department,  Federal  University  of  Technology,  Minna, Niger  State.  I  have  demonstrated  excellent  skills  in  numerous  aspects  of  Information  and  Communication Technology.  A  very  good  experience  in  Local  Area  Network  implementation  and  management,  from  a  simple network to a mid­level complex network scenario of not less than one thousand Workstations (Microsoft Windows Vista  and  Microsoft  Windows  XP)  with  Microsoft  Windows  2008  Server  R2  Active  Directory  Domain  Controller deployed  in  more  than  single  location.  VPN,  WAN  link  optimization,  Firewall  &#38;  Intrusion  Detection  System, Web/Email hosting control panel, OpenNMS network management application.</p--> 
<p>
My name is Mosudi lsiaka, I am 40 years old, married with three children. I am a graduate of Electrical and Computer Engineering Department, Federal University of Technology Minna,Nigeria. I have demonstrated excellent skills in numerous aspects of Information and Communications Technology. A very good experience in Local Area Network implementation and management, from a simple network to a mid-level complex network scenario of not less than a thousand Workstations with Microsoft Windows Server Active Directory Domain Controller deployed in more than single location. I have the the capacity to deploy Data centre infrastructures ranging from Firewall & Intrusion Detection System, Web/Email Hosting Control Panel, VPN Gateway, Network Management Systems, Virtualization and cloud computing, Routing, Link Bonding, Redundancy, Fail-over and Clustering, to WAN link optimization with available off the shelf hardware servers at little or zero additional cost using open source software to deliver enterprise grade network solutions. 
<br>
I have within the last 20 months Setup KVM, I mean Kernel-based Virtual Machine, virtualization infrastructure on Ubuntu Linux server at in house data center and MTN Nigeria Data Center facility with own storage replication of virtual machines at a remote data centre colocation facility in United Kingdom using GlusterFS, glusterfs is an open source, distributed file system capable of scaling to several petabytes and handling thousands of clients. Provide secure online availability of Servers (hardware and virtual machines) services and applications. Network and System Monitoring and Management platform using Icinga2. I have running a couple of Microsoft Windows Server 2008 R2 virtual machines behind FreeBSD virtual machine as a firewall gateway. Setup Zimbra Collaboration Suite groupware email server. I equally setup Web and Email hosting control panel.

</p>

   
         
        <p>Reading technical journals/books; this has helped me gain knowledge in various
issues of life like time management and personal development/effectiveness. I enjoy watching news and documentaries; it imbibes in me the values of being conscious of 
my environment, and persistence portrayed by unfolding news events and established facts 
in documentaries.</p>
                         
                        </div><!--//content-->
                    </div><!--//section-inner-->                 
                </section><!--//section-->
    
               <section class="latest section">
                    <div class="section-inner">
                        <h2 class="heading">Latest Project</h2>
                        <div class="content">    
                                               
                            <div class="item featured text-center">
                                <h3 class="title"><a href="https://releases.openstack.org/ocata/index.html" >Cloud Computing - OpenStack®</a></h3>
                                <p class="summary">Implementation of OpenStack Ocata - Cloud Computing</p>
                                <div class="featured-image">
                                    <a href="http://mioemi.com/#" >
                                    <img class="img-responsive project-image" src="assets/images/projects/project-featured.png" alt="project name" />
                                    </a>
                                    <div class="ribbon">
                                        <div class="text">MioCloud</div>
                                        </div>
                                    </div>
                                    
                                <div class="desc text-left" >                                    
                                    <p>Open source software for creating private and public clouds.

OpenStack software controls large pools of compute, storage, and networking resources throughout a datacenter, managed through a dashboard or via the OpenStack API. OpenStack works with popular enterprise and open source technologies making it ideal for heterogeneous infrastructure.</p>
                                    <p>OpenStack® delivers a common architecture that is open, secure and flexible across private, public, and hybrid clouds.</p>
                                </div><!--//desc-->         
                                <!--a class="btn btn-cta-secondary" href="http://mioemi.com/#" target="_blank"><i class="fa fa-thumbs-o-up"></i> Back my project</a-->  

<input type="checkbox" name="additional" id="additional">
    <label for="additional"><h4 class="alert alert-info" ><i class="fa fa-thumbs-o-up"></i> Click To Back My Project</h4></label>
  
    <div class="reveal-if-active">
   
<div class="list-group col-xs-12">

<form action="" method="post" id="form" >
<div class="list-group-item active" style="background-color:#05692B;font-weight:bold;height: 30px;">Contact Details </div>
<div class="row no-gutters">
<div class="col-md-3">
<div class="list-group-item" style="height: 90px;color:#05692B ;font-weight:bold;text-align:left">Name </div>
<div class="list-group-item" style="height: 70px;color:#05692B ;font-weight:bold;text-align:left"> Address </div>
<div class="list-group-item" style="height: 90px;color:#05692B ;font-weight:bold;text-align:left">Mobile </div>
<div class="list-group-item" style="height: 70px;color:#05692B ;font-weight:bold;text-align:left">Contact Email </div>
<div class="list-group-item" style="height: 95px;color:#05692B ;font-weight:bold;text-align:left"><img class="img-responsive project-image" src="assets/images/projects/not_robot.jpg" alt="Captcha" />
 </div>


</div>



<div class="col-md-9" >
<div class="list-group-item" style="height: 90px;text-align:left">
<div class="row">
<div class="col-md-3">
    <div class="form-group">
      	<label for="Prefix">Prefix</label>
        <select style="height:50px" class="form-control require-if-active" name="prefix" id="prefix" data-require-pair="#additional" >
          <option value="Miss">Miss</option>
          <option value="Mrs">Mr</option>
          <option value="Mrs">Mrs</option>
          <option value="Engr.">Engr.</option>
          <option value="Dr">Dr</option>
          <option value="Prof">Prof</option>
          <option value="Chief">Chief</option>
        </select>
    </div></div>

<div class="col-md-7">
<div class="form-group">
    <label for="Fullname">Name</label>
    <input type="text" style="height:50px" class="form-control" name="Fullname" id="Fullname" placeholder="Olutayo Ndagi Ebuka" value="<?php if(isset($_POST['Fullname'])) { echo htmlentities ($_POST['Fullname']); }?>" required>
</div></div>
</div>
</div>

<div class="list-group-item" style="height: 70px;text-align:left">
<input type="text" class="form-control" name="ContactAddress" id="ContactAddress" style="height:50px" placeholder =" Contact address" value="<?php if(isset($_POST['ContactAddress'])) { echo htmlentities ($_POST['ContactAddress']); }?>" >
</div>

<div class="list-group-item" style="height: 90px;text-align:left">
<div class="row"><div class="col-md-5">
<div class="form-group">
    <label for="mobile1">Direct Line</label>
    <input type="text" class="form-control require-if-active" style="height:50px" name="Mobile1" id="Mobile1" placeholder="Telephone number"  value="<?php if(isset($_POST['Mobile1'])) { echo htmlentities ($_POST['Mobile1']); }?>"  data-require-pair="#additional">
<script language="JavaScript">
$(function(){
  $('#Mobile1').keypress(function(e){
    if(e.which == 48 || e.which == 49 || e.which == 50 || e.which == 51 || e.which == 52 || e.which == 53 || e.which==54 || e.which==55 || e.which==56|| e.which==57) {
    } else {
      return false;
    }
  });
});
</script>
</div></div><div class="col-md-6">
<div class="form-group">
    <label for="mobile2">Alternative Line</label>
    <input type="text" class="form-control" style="height:50px" name="Mobile2" id="Mobile2" placeholder="Optional Telephone number" value="<?php if(isset($_POST['Mobile2'])) { echo htmlentities ($_POST['Mobile2']); }?>">
<script language="JavaScript">
$(function(){
  $('#Mobile2').keypress(function(e){
    if(e.which == 48 || e.which == 49 || e.which == 50 || e.which == 51 || e.which == 52 || e.which == 53 || e.which==54 || e.which==55 || e.which==56|| e.which==57) {
    } else {
      return false;
    }
  });
});
</script>
</div></div></div>
</div>

<div class="list-group-item" style="height: 70px;text-align:left"><div class="row"><div class="col-md-8"><input type="text" style="height:50px;text-transform:lowercase" class="form-control require-if-active" id="ContactEmail" name="ContactEmail" placeholder=" Email" value="<?php if(isset($_POST['ContactEmail'])) { echo htmlentities ($_POST['ContactEmail']); }?>"  data-require-pair="#additional" ></div></div>
</div>


<div class="list-group-item" style="height: 95px;text-align:left">
<div class="row">
<div class="col-md-8"><div class="g-recaptcha" data-sitekey="6LfTRwsUAAAAAJVQ4SfIfVYM2NdHlpsZ__-OvmZq"></div> </div>
<div class="col-md-4"><button type="reset" class="btn btn-danger  btn-lg">Cancel</button></div>
<div class="col-md-4"><button type="submit" name="submit" value="Submit" class="btn btn-success btn-lg">Submit</button></div>
</div></div></div>
</form>
</div><!--row no-gutters-->
</div><!--close reveal-if-active class -->
</div><!--close option div-->
<?php 

if(isset($_POST['submit'])){

if(isset($_POST['g-recaptcha-response']))
          $captcha=$_POST['g-recaptcha-response'];
 if(!$captcha){
         
   echo "<script>";
   echo "alert('Please check the the captcha form.!');";
   //echo " return false; ";
   //echo ' $("#additional").click() ';

   echo " window.location='#form'; ";
   echo ' $("#additional").prop("checked", true); ';   

   echo "</script>";
 
   $notRobot = " I'm not a robot ";

   echo ' <p class="text-danger">Please Click the box(<strong>' .$notRobot. '</strong>) above to submit the form</p> ';

   //      exit;
         
	$gohere1 = "index.php#form";
    	header ("Location: $gohere1");
 }
        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfTRwsUAAAAAPnOA3Pl7jP4kdvJYi3RnuoGe7yS&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false)

 {
          //echo '<h2>You are spammer ! Get the @$%K out</h2>';
echo "<script type='text/javascript'>
    $(window).load(function(){
        $('#myModal').modal('show');
    });
</script>" ;

	$gohere1 = "index.php#form";
    	header ("Location: $gohere1");
   } else    
{
    $to = "imosudi@gmail.com"; // this is your Email address
    $from = $_POST['ContactEmail']; // this is the sender's Email address
    $Fullname = $_POST['Fullname'];
    $last_name = $_POST['ContactAddress'];
    $subject = "Back My Project";
    $prefix = $_POST['prefix'];
    //$subject2 = "Copy of your form submission";
    $message = $prefix . $Fullname . " " . $last_name . " wrote the following:" . "\n\n" . $_POST['Mobile1'];
    //$message2 = "Here is a copy of your message " . $Fullname . "\n\n" . $_POST['ContactAddress'];

    $headers = "From:" . $from;
    //$headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    //mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    //header('Location: #additional');


/***$htmlString= '<div class="alert alert-dismissible alert-info">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong><quote>Great friends, friends indeed are known when in need.
That is who you are. </quote>!</strong> Thank you so much '.$prefix." " . $Fullname . ' for your support. I will contact you shortly.
</div>';***/

echo '<div class="alert alert-dismissible alert-info">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong><quote>Great friends, friends indeed are known when in need.
That is who you are. </quote>!</strong> Thank you so much '.$prefix." " . $Fullname . ' for your support. I will contact you shortly.
</div>';

    // I can also use header('Location: thank_you.php'); to redirect to another page.
    }
}
?>
<!-- tracking changes in computer files -->
               
                            </div><!--//item-->
                            <hr class="divider" />

<div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href=#" >
                                <img class="img-responsive project-image" src="http://www.agilenutshell.com/assets/test-driven-development/tdd-circle-of-life.png" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="#">TDD- Test-driven development (TDD)</a></h3>
                                    <p>Test-driven development (TDD) using Python from a beginner point of view.
					</p>
					<a href="https://github.com/imosudi/testdrivendev_with_python"> https://github.com/imosudi</a>
					
                                    <p><a class="more-link" href="#" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->

<div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="http://mioemi.com/ansible" >
                                <img class="img-responsive project-image" src="assets/images/projects/git_ansible_icinga_docker_AWS.png" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="http://mioemi.com/git_ansible">Ansible- Automate my IT solutions going forward</a></h3>
                                    <p>Automation with Ansible while source code management using Git, removes the complexity that comes with every IT environment. I will be automating the whole process from provisioning of ec2 AWS Ubuntu server instance to installation of Docker, Icinga2, Icingaweb2 with dependencies, prerequisite application suits, systems update and swap creation and action..</p>
                                    <p><a class="more-link" href="http://mioemi.com/ansible" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->


<!--div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="http://mioemi.com/ansible" >
                                <img class="img-responsive project-image" src="assets/images/projects/ansible_icinga_docker_AWS.png" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="http://mioemi.com/ansible" >Asinble- Automate my IT solutions going forward</a></h3>
                                    <p>Automation with Ansible removes the complexity that comes with every IT environment. I will be automating the whole process from provisioning of ec2 AWS Ubuntu server instance to installation of Docker, Icinga2, Icingaweb2 with dependencies, prerequisite application suits, systems update and swap creation and action..</p>
                                    <p><a class="more-link" href="http://mioemi.com/ansible" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc->                          
                            </div><!--//item-->

<div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="http://mioemi.com/docker" >
                                <img class="img-responsive project-image" src="assets/images/projects/icinga_docker_AWS.png" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="http://mioemi.com/icinga2" >ICINGA 2- The best of opensource data center infrastructure monitoring</a></h3>
                                    <p>Icinga 2 features a new configuration format that is intuitive to write, efficient to execute and even adjusts to the changing conditions of your environment at run-time (This Deployment document remains valid for use on Ubuntu 14.04.4 LTS...</p>
                                    <p><a class="more-link" href="http://mioemi.com/docker" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->


<div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="http://mioemi.com/#" >
                                <img class="img-responsive project-image" src="assets/images/projects/docker_on_AWS.png
" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="http://mioemi.com/docker" >Docker  Installation on AWS Instance</a></h3>
                                    <p>Docker installation on Ubuntu Linux server AWS Instance. Docker containerization is a little different than hypervisor-based server virtualization. But encapsulating an application in a container with an operating environment achieves many of the benefits of loading and application onto a virtual machine (This Deployment document remains valid for use on Ubuntu 14.04.4 LTS...</p>
                                    <p><a class="more-link" href="http://mioemi.com/docker" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->
                            

                            <div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="http://mioemi.com/#" >
                                <img class="img-responsive project-image" src="assets/images/projects/project-4.jpg" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="http://mioemi.com/#" >One Network  - Data Center Onboarding</a></h3>
                                    <p>KVM virtualisation infrastructure on Ubuntu Linux server at MTN Nigeria collocation facility with own storage replication of virtual machines at a remote data centre collocation facility in UK using GlusterFS. Provide secure online availability of Servers(virtual machines) services and applications (Backend and...</p>
                                    <p><a class="more-link" href="http://mioemi.com/#" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->
                            
                            <div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="http://mioemi.com/#" >
                                <img class="img-responsive project-image" src="assets/images/projects/project-1.png" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="http://mioemi.com/firewall.html" >ChamsCity - Network Infrastructure and Management</a></h3>
                                    <p>Microsoft Active Directory Domain Controller using Windows server 2008 R2 and the integration of not less that one thousand Workstations(Microsoft Windows Vista and Microsoft Windows XP operating systems) at the two ChamsCity Digital Mall locations (ChamsCity, Ikeja and ChamsCity, Central Business District, Abuja) Firewall with advance web filtering using a FreeBSD ... </p>
                                    <p><a class="more-link" href="http://mioemi.com/firewall.html" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->
                            
                            <div class="item row">
                                <a class="col-md-4 col-sm-4 col-xs-12" href="https://www.google.com/mapmaker?fll=6.476338,3.724365&fr=0.109132,0.109863&mpp=303.713165&gw=12&ll=6.584732,3.443527&spn=0.727131,1.679535&z=10&lyt=large_map_v3&hll=6.424988,3.480242&hyaw=77.95565888490519" target="_blank">
                                <img class="img-responsive project-image" src="assets/images/projects/project-2.png" alt="project name" />
                                </a>
                                <div class="desc col-md-8 col-sm-8 col-xs-12">
                                    <h3 class="title"><a href="wireless.html" >LASRRA - Long Distance Point-to-Point</a></h3>
                                    <p> A single hop point to point wireless link between Lagos State Ministry of Science and Technology, State Secretariat, Alausa, Ikeja and Library, Sangotedo, Eti Osa Local Government Area, Lagos. The link span is about 35 kilometer...</p>
                                    <p><a class="more-link" href="wireless.html" ><i class="fa fa-external-link"></i> Find out more</a></p>
                                </div><!--//desc-->                          
                            </div><!--//item-->
                            
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->
                
                <section class="projects section">
                    <div class="section-inner">
                        <h2 class="heading">Other Projects</h2>
                        <div class="content">
                            <div class="item">
                                <h3 class="title"><a href="#">Open Source Routing Platform</a></h3>
                                <p class="summary">There was a need for high resource router with capacity to handle network traffic for more than a thousand connected workstations, with not less than 30 access layer switches, about ten aggregation layer switches as well as a core switch...</p>
                                <p><a class="more-link" href="http://mioemi.com/#" ><i class="fa fa-external-link"></i> Find out more</a></p>
                            </div><!--//item-->
                            <div class="item">
                                <h3 class="title"><a href="#">Open Source Firewall</a> <span class="label label-theme">Open Source</span></h3>
                                <p class="summary">Firewall, NAT, Port forwarding, VPN, IDS and redundancy/failover.
Quick on your mind are:
<br> . Cisco PIX (Private Internet Exchange). 
<br> . Assure Managed Firewall
</p>
                                <p><a class="more-link" href="http://mioemi.com/#" ><i class="fa fa-external-link"></i> View on GitHub</a></p>
                            </div><!--//item-->
                            <div class="item">
                                <h3 class="title"><a href="#">Web/Mail Hosting Control Panel</a> <span class="label label-theme">Open Source</span></h3>
                                <p class="summary">
<ul>
<li>Manage multiple servers from one control panel</li>
<li>Web server management (Apache2 and nginx)</li>
<li>Mail server management (with virtual mail users)</li>
<li>DNS server management (BIND and MyDNS)</li>
<li>Virtualization (OpenVZ)</li>
<li>Administrator, reseller and client login</li></ul></p>
                                <p><a class="more-link" href="http://mioemi.com/#" ><i class="fa fa-external-link"></i> View on GitHub</a></p>
                            </div><!--//item-->
                            
                            <a class="btn btn-cta-secondary" href="#">More on CoderWall <i class="fa fa-chevron-right"></i></a>
                            
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->
                
                <section class="experience section">
                    <div class="section-inner">
                        <h2 class="heading">Work Experience</h2>
                        <div class="content">
                            <div class="item">
                                <h3 class="title">Data Centre Manager - <span class="place"><a href="#">One Network Management Services Ltd.</a></span> <span class="year">(2012 - Present)</span></h3>
                                <p>Responsibilities  included  install,  configure,  and  support  the  LANs,  WANs  and  Internet  systems,  Data  Center Collocation On­boarding, maintain network  and  workstation  hardware  and  software,  monitor  network  to  ensure    network  availability    to    all  system  users  and  perform  necessary  maintenance  to  support  network availability,  supervise  and  train  network  support  technicians  and  coordinate  network  security  measures, manage  capacity  planning  activities  to  enhance  the  performance  of  the  network  resources,  perform routine backups  and archival  of files on the network to assist with  disaster  recovery,  analyse  user support  statistics and  other  data  and  recommend  appropriate  measures,  assists    in   maintaining    the    operating    system    and   security  software  utilized  on  the network,  Interract  with vendors and service providers, responsibility for effective cost management of the organization expenditure on Network/Server infrastructures.</p>
                            </div><!--//item-->
                            <div class="item">
                                <h3 class="title">Network Engineer/Administrator - <span class="place"><a href="#">Daybreak Afrika Technologies</a></span> <span class="year">(2006 - 2011)</span></h3>
                                <p>Responsibilities  included  responsibility  for  technical  operations,  provide  support  in  resolving  network  problems either  directly  on  site  or  via  telephone,  engineer,  design  and  conduct  trade  off  studies  on  network  projects, Conducting  site  surveys.  Maintenance/service  support  of  installed  locations,  training  on  wireless/wired  LAN Technology and Linux server administration.</p>
                            </div><!--//item-->
                            
                            <div class="item">
                                <h3 class="title">Wireless Network Engineer/Administrator - <span class="place"><a href="#">Naira.Com Nigeria Ltd</a></span> <span class="year">(2004 - 2005)</span></h3>
                                <p>Responsibilities included Conducting site surveys, Deploying Wi­fi solutions at clients’ premises, Configuring Wireless access points, Maintenance/service support of installed locations, Preparation of training and certification materials, Responsibility to technical operations manager, Engineer, design and conduct trade off studies on network projects for clients, Implementing and administering IEEE 802.11 family standard of radio network using off shelf and proprietary radio and devices both in the license­free and licensed band, Provide support in resolving network problems either directly on site or via telephone, Implementing and administering
Microsoft family of operating systems.</p>
                            </div><!--//item-->
                            
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </section><!--//section-->
                


<section class="github section">
                    <div class="section-inner">
                        <h2 class="heading">My GitHub</h2>

<!--
                        <p>You can embed your GitHub activities using Casey Scarborough's <a href="http://caseyscarborough.com/projects/github-activity/" target="_blank">GitHub Activity Stream</a> widget. You can also take a screenshot of your GitHub chart and put it here for fun.</p>
                        <p><a href="#"><img class="img-responsive" src="assets/images/github-chart.png" alt="GitHub Contributions Chart" /></a></p>

-->               
                        <!--//Usage: http://caseyscarborough.com/projects/github-activity/ -->                       
   

                     <div id="ghfeed" class="ghfeed">  </div><!--//ghfeed-->
<!--added by mosud-->
<script>
GitHubActivity.ghfeed({
    username: "imosudi",
    //selector: "#ghfeed",
    limit: 20 // optional
});
</script>
<!--end as added by mosud-->
                      
                        
                    </div><!--//secton-inner-->
                </section><!--//section-->
            </div><!--//primary-->



            <div class="secondary col-md-4 col-sm-12 col-xs-12">
                 <aside class="info aside section">
                    <div class="section-inner">
                        <h2 class="heading sr-only">Basic Information</h2>
                        <div class="content">
                            <ul class="list-unstyled">
                                <li><i class="fa fa-map-marker"></i><span class="sr-only">Location:</span>Lagos, Nigeria & Remote</li>
                                <li><i class="fa fa-envelope-o"></i><span class="sr-only">Email:</span><a href="#">info@mioemi.com</a></li>
                                <li><i class="fa fa-link"></i><span class="sr-only">Website:</span><a href="#">http://www.mioemi.com</a></li>
				<li><!--i class="fa fa-phone"></i><span class="sr-only">Telephone:</span><a href="#"--><div class="row"><div class="col-md-1"><i class="fa fa-phone"></i><span class="sr-only">Telephone:</span><a href="#"></div><div class="col-md-5" style="font-size:12px">+2348098673498</div> <div col-md-1></div> <div class="col-md-4" style="font-size:12px" >+2348053673498</div></div></a></li>
                            </ul>
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </aside><!--//aside-->
                
                <aside class="skills aside section">
                    <div class="section-inner">
                        <h2 class="heading">Skills</h2>
                        <div class="content">
                            <p class="intro">
                                I  have  demonstrated  excellent  skills  in  numerous  aspects  of  Information  and  Communication Technology.  A  very  good  experience  in  Local  Area  Network  implementation  and  management,  from  a  simple network to a mid­level complex network scenario of not less than one thousand Workstations deployed  in  more  than  single  location. </p>
                            
                            <div class="skillset">
                              

                                <div class="item">
                                    <h3 class="level-title">System Automation( Ansible)<span class="level-label" data-toggle="tooltip" data-placement="left" data-animation="true" title="You can use the tooltip to add more info...">Expert</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="52%">
                                        </div>                                   
                                    </div><!--//level-bar-->                     
                                </div><!--//item-->


 
                                <div class="item">
                                    <h3 class="level-title">Python <span class="level-label" data-toggle="tooltip" data-placement="left" data-animation="true" title="You can use the tooltip to add more info...">Expert</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="52%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->
                                
                                <div class="item">
                                    <h3 class="level-title">TCP/IP Networking<span class="level-label">Expert</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="96%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->
                                
                                <div class="item">
                                    <h3 class="level-title">Network Management &amp; Traffic Engineering Systems<span class="level-label">Expert</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="96%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->


<div class="item">
                                    <h3 class="level-title">Storage Management<span class="level-label">Expert</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="76%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->

                                
                                <div class="item">
                                    <h3 class="level-title">Data Center Collocation Onboarding <span class="level-label">Pro</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="95%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->



<div class="item">
                                    <h3 class="level-title">Linux Server Administration<span class="level-label">Pro</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="85%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->




<div class="item">
                                    <h3 class="level-title">Web & Mail Hosting Infrastructure<span class="level-label">Pro</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="85%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->




<div class="item">
                                    <h3 class="level-title">Virtualization<span class="level-label">Pro</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="85%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->



<div class="item">
                                    <h3 class="level-title">Long Distance Wireless<span class="level-label">Pro</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="85%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->

<div class="item">
                                    <h3 class="level-title">Web Design<span class="level-label">Expert</span></h3>
                                    <div class="level-bar">
                                        <div class="level-bar-inner" data-level="60%">
                                        </div>                                      
                                    </div><!--//level-bar-->                                 
                                </div><!--//item-->
                                
                                <p><a class="more-link" href="http://mioemi.com/resume.html"><i class="fa fa-external-link"></i> More on my CV</a></p> 
                            </div>              
                        </div><!--//content-->  
                    </div><!--//section-inner-->                 
                </aside><!--//section-->
                
                <aside class="testimonials aside section">
                    <div class="section-inner">
                        <h2 class="heading">Testimonials</h2>
                        <div class="content">
                            <div class="item">
                                <blockquote class="quote">                                  
                                    <p><i class="fa fa-quote-left"></i>Isiaka is a great guy with a positive entrepreneurship spirit. He has a flair for quality and is ready to get the job done regardless of obstacles. A great guy to hire</p>
                                </blockquote>                
                                <p class="source"><span class="name">Akeem Akinola</span><br /><span class="title">Senior Software Engineer at Cerner Corporation</span></p>                                                             
                            </div><!--//item-->
                            
                            <p><a class="more-link" href="http://ng.linkedin.com/pub/isiaka-mosudi/1b/7a2/936/"><i class="fa fa-external-link"></i> More on Linkedin</a></p> 
                            
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                
                <aside class="education aside section">
                    <div class="section-inner">
                        <h2 class="heading">Education</h2>
                        <div class="content">
                            <div class="item">                      
                                <h3 class="title"><i class="fa fa-graduation-cap"></i> B.Engr Electrical and Computer Engineering</h3>
                                <h4 class="university">Federal University of Technology, Minna <span class="year">(1998-2003)</span></h4>
                            </div><!--//item-->
                            <div class="item">
                                <h3 class="title"><i class="fa fa-graduation-cap"></i> Certificate: The Science of Everyday Thinking</h3>
                                <h4 class="university">UQx, University of Queensland<span class="year">(2014)</span></h4>
                            </div><!--//item-->
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                            
                <aside class="languages aside section">
                    <div class="section-inner">
                        <h2 class="heading">Languages</h2>
                        <div class="content">
                            <ul class="list-unstyled">
                                <li class="item">
                                    <span class="title"><strong>Yoruba:</strong></span>
                                    <span class="level">Native Speaker <br class="visible-xs"/><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                                </li><!--//item-->
                                <li class="item">
                                    <span class="title"><strong>English:</strong></span>
                                    <span class="level">Fluent &amp; Profnl Proficiency<br class="visible-xs"/><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span>
                                </li><!--//item-->
                            </ul>
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                
                <aside class="blog aside section">
                    <div class="section-inner">
                        <h2 class="heading">Latest Blog Posts</h2>
                        <!-- <p>You can use Sascha Depold's <a href="https://github.com/sdepold/jquery-rss" target="_blank">jQuery RSS plugin</a> to pull in your blog post feeds.</p> -->
                        <div id="rss-feeds" class="content">



<!-- 
	         
	<script>
          jQuery(function($) {
            $("#rss-feeds").rss("http://feeds.feedburner.com/blogspot/uEOFe")
          })
        </script>
	     
	</head>
      <body>
	
        <div id="rss-feeds"></div>
	-->




                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                
                <aside class="list music aside section">
                    <div class="section-inner">
                        <h2 class="heading">Favourites</h2>
                        <div class="content">
                            <ul class="list-unstyled">
                                <li><i class="fa fa-headphones"></i> <a href="#">Desktop: Ubuntu LTS Desktop </a></li>
                                <li><i class="fa fa-headphones"></i> <a href="#">Server: Ubuntu LTS Server </a></li>
                                <li><i class="fa fa-headphones"></i> <a href="#">Language: Python and HTML</a></li>
                                
                            </ul>
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                
                <aside class="list conferences aside section">
                    <div class="section-inner">
                        <h2 class="heading">Conferences</h2>
                        <div class="content">
                            <ul class="list-unstyled">
                                <li><i class="fa fa-calendar"></i> <a href="https://developer.apple.com/wwdc/" target="_blank">WWDC 2014</a> (Lagos)</li>
                                <li><i class="fa fa-calendar"></i> <a href="http://hive.aigaseattle.org/">Hive</a> (Seattle)</li>
                            </ul>
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
                
                <aside class="credits aside section">
                    <div class="section-inner">
                        <h2 class="heading">Credits</h2>
                        <div class="content">
                            <ul class="list-unstyled">
                                <li><a href="http://getbootstrap.com/" target="_blank"><i class="fa fa-external-link"></i> Bootstrap 3.2</a></li>
                                <li><a href="http://fortawesome.github.io/Font-Awesome/" target="_blank"><i class="fa fa-external-link"></i> FontAwsome 4.1</a></li>
                                <li><a href="http://jquery.com/" target="_blank"><i class="fa fa-external-link"></i> jQuery</a></li>
                               
                                                              
                                
                            </ul>
                            
                            <hr/>
                            
                             <p>This responsive HTML5 CSS3 site template is handcrafted by UX designer <a href="https://www.linkedin.com/in/xiaoying" target="_blank">Xiaoying Riley</a> at <a href="http://themes.3rdwavemedia.com/" target="_blank">3rd Wave Media</a> for developers and is <strong>FREE</strong> under the <a class="dotted-link" href="http://creativecommons.org/licenses/by/3.0/" target="_blank">Creative Commons Attribution 3.0 License</a></p>
                             
                            <a class="btn btn-cta-secondary btn-follow" href="https://twitter.com/mioemi2000" target="_blank"><i class="fa fa-twitter"></i> Follow me</a>
                            
                        </div><!--//content-->
                    </div><!--//section-inner-->
                </aside><!--//section-->
              
            </div><!--//secondary-->    
        </div><!--//row-->
    </div><!--//masonry-->
    
    <!-- ******FOOTER****** --> 
    <footer class="footer">
        <div class="container text-center">
                <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://mioemi.com" >Mosudi Isiaka</a> Mioemi!</small>
        </div><!--//container-->
    </footer><!--//footer-->
 
    <!-- Javascript -->      
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>    
     <script type="text/javascript" src="assets/plugins/jquery-rss/dist/jquery.rss.min.js"></script> 
    <!-- github activity plugin -->
    <script type="text/javascript" src="assets/mioemi_script/mustache.min.js"></script>
    <script type="text/javascript" src="assets/mioemi_script/github-activity-0.1.5.min.js"></script>
    <!-- custom js -->
    <script type="text/javascript" src="assets/js/main.js"></script> 

<div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Thank you for pre-registering!</h4>
      </div>
      <div class="modal-body">
          <p>Thanks for getting in touch!</p>                     
      </div>    
    </div>
  </div>
</div>

</body>
</html> 
