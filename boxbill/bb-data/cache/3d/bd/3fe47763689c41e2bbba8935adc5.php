<?php

/* partial_footer.phtml */
class __TwigTemplate_3dbd3fe47763689c41e2bbba8935adc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    \t<span>&copy; Copyright ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["now"]) ? $context["now"] : null), "Y"), "html", null, true);
        echo ". All rights reserved. Powered by <a href=\"http://www.boxbilling.com\" title=\"Billing system\" target=\"_blank\">BoxBilling ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "system_version"), "html", null, true);
        echo "</a>
                    ";
        // line 2
        if (constant("BB_DEBUG")) {
            // line 3
            echo "                    <a href=\"http://www.boxbilling.com/forum\" title=\"\" target=\"_blank\" style=\"float:right;\">Report bug</a>
                    ";
        }
        // line 5
        echo "        </span>
";
    }

    public function getTemplateName()
    {
        return "partial_footer.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 5,  24 => 2,  26 => 3,  22 => 2,  17 => 1,  114 => 41,  109 => 4,  103 => 50,  97 => 47,  95 => 46,  89 => 42,  87 => 41,  71 => 33,  67 => 32,  59 => 29,  34 => 9,  32 => 8,  25 => 4,  20 => 1,  142 => 57,  133 => 51,  129 => 50,  119 => 49,  115 => 44,  105 => 39,  101 => 49,  93 => 34,  84 => 28,  74 => 23,  70 => 22,  60 => 17,  56 => 16,  48 => 21,  46 => 11,  41 => 9,  36 => 6,  33 => 5,  27 => 3,);
    }
}
