<?php

/* partial_menu_top.phtml */
class __TwigTemplate_369de5045ecde700844c8b436e85b757 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "                        <ul class=\"menu_body\">
                            ";
        // line 2
        if ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "order")), "method")) {
            // line 3
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("order#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Order");
            echo "</a></li>
                            ";
        }
        // line 5
        echo "                            
                            ";
        // line 6
        if ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "invoice")), "method")) {
            // line 7
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("invoice#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Invoice");
            echo "</a></li>
                            ";
        }
        // line 9
        echo "                            
                            ";
        // line 10
        if ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "client")), "method")) {
            // line 11
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("client#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Client");
            echo "</a></li>
                            ";
        }
        // line 13
        echo "                            
                            ";
        // line 14
        if ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "product")), "method")) {
            // line 15
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("product#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Product");
            echo "</a></li>
                            ";
        }
        // line 17
        echo "                            
                            ";
        // line 18
        if ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "support")), "method")) {
            // line 19
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("support#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Support ticket");
            echo "</a></li>
                            ";
        }
        // line 21
        echo "                            
                            ";
        // line 22
        if ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "staff")), "method")) {
            // line 23
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("staff#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Staff member");
            echo "</a></li>
                            ";
        }
        // line 25
        echo "                            
                            ";
        // line 26
        if (($this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "extension_is_on", array(0 => array("mod" => "news")), "method") && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "news")), "method"))) {
            // line 27
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("news#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Announcement");
            echo "</a></li>
                            ";
        }
        // line 29
        echo "                            
                            ";
        // line 30
        if (($this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "extension_is_on", array(0 => array("mod" => "forum")), "method") && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_is_allowed", array(0 => array("mod" => "forum")), "method"))) {
            // line 31
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, twig_bb_admin_link_filter("forum#tab-new"), "html", null, true);
            echo "\" title=\"\">";
            echo gettext("Forum topic");
            echo "</a></li>
                            ";
        }
        // line 33
        echo "                        </ul>";
    }

    public function getTemplateName()
    {
        return "partial_menu_top.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 33,  113 => 31,  108 => 29,  100 => 27,  98 => 26,  87 => 23,  85 => 22,  72 => 18,  69 => 17,  61 => 15,  59 => 14,  56 => 13,  46 => 10,  43 => 9,  33 => 6,  22 => 3,  24 => 3,  646 => 135,  636 => 127,  625 => 126,  607 => 122,  602 => 121,  597 => 118,  586 => 117,  572 => 114,  561 => 113,  540 => 111,  527 => 108,  521 => 106,  518 => 105,  515 => 104,  512 => 103,  509 => 102,  506 => 101,  503 => 100,  500 => 99,  497 => 98,  494 => 97,  492 => 96,  489 => 95,  487 => 94,  484 => 93,  481 => 92,  478 => 91,  475 => 90,  472 => 89,  457 => 87,  437 => 82,  434 => 81,  431 => 80,  428 => 79,  425 => 78,  416 => 75,  407 => 72,  376 => 66,  363 => 62,  351 => 60,  340 => 56,  323 => 51,  304 => 49,  290 => 46,  269 => 40,  261 => 38,  251 => 36,  249 => 35,  229 => 32,  209 => 26,  204 => 23,  195 => 22,  183 => 19,  166 => 17,  153 => 15,  139 => 14,  110 => 9,  105 => 8,  75 => 5,  60 => 2,  49 => 1,  44 => 125,  41 => 116,  38 => 112,  35 => 7,  29 => 69,  26 => 31,  23 => 21,  20 => 2,  17 => 1,  469 => 88,  464 => 124,  459 => 123,  452 => 117,  443 => 84,  440 => 83,  435 => 109,  430 => 106,  424 => 105,  419 => 76,  414 => 100,  409 => 25,  404 => 71,  394 => 137,  392 => 70,  387 => 133,  385 => 132,  374 => 125,  369 => 123,  364 => 120,  362 => 114,  354 => 109,  350 => 107,  347 => 106,  345 => 105,  341 => 104,  338 => 103,  334 => 101,  332 => 100,  329 => 99,  327 => 98,  313 => 89,  310 => 88,  302 => 86,  297 => 84,  284 => 42,  271 => 81,  265 => 39,  263 => 78,  246 => 34,  243 => 74,  241 => 33,  238 => 72,  224 => 70,  221 => 69,  219 => 68,  199 => 64,  197 => 63,  186 => 59,  174 => 50,  158 => 48,  147 => 44,  141 => 43,  135 => 41,  133 => 40,  130 => 39,  128 => 38,  124 => 37,  120 => 36,  106 => 33,  95 => 25,  90 => 6,  82 => 21,  78 => 20,  74 => 19,  70 => 18,  66 => 17,  62 => 16,  58 => 15,  55 => 14,  53 => 13,  50 => 12,  48 => 11,  39 => 7,  32 => 86,  30 => 5,  864 => 291,  860 => 290,  856 => 289,  852 => 288,  848 => 287,  841 => 282,  839 => 281,  836 => 280,  833 => 279,  828 => 276,  818 => 268,  809 => 264,  807 => 263,  802 => 260,  784 => 255,  776 => 254,  772 => 253,  760 => 252,  753 => 251,  734 => 250,  732 => 249,  724 => 243,  715 => 239,  713 => 238,  708 => 235,  690 => 230,  682 => 229,  678 => 228,  666 => 227,  659 => 226,  640 => 225,  638 => 224,  626 => 215,  622 => 214,  618 => 212,  616 => 211,  613 => 210,  603 => 203,  591 => 194,  579 => 185,  567 => 176,  555 => 167,  541 => 156,  533 => 155,  524 => 107,  514 => 146,  510 => 144,  508 => 143,  505 => 142,  495 => 134,  486 => 131,  483 => 130,  471 => 127,  461 => 126,  458 => 125,  453 => 124,  446 => 116,  442 => 119,  433 => 113,  422 => 77,  413 => 74,  410 => 73,  396 => 97,  388 => 96,  383 => 95,  378 => 127,  371 => 124,  367 => 89,  356 => 110,  352 => 82,  346 => 78,  344 => 77,  342 => 76,  339 => 75,  328 => 54,  322 => 68,  316 => 67,  308 => 66,  300 => 48,  296 => 64,  288 => 61,  282 => 60,  276 => 59,  268 => 80,  260 => 77,  256 => 56,  248 => 53,  242 => 52,  236 => 51,  228 => 50,  220 => 49,  216 => 67,  208 => 45,  202 => 65,  196 => 43,  188 => 42,  180 => 41,  176 => 40,  168 => 49,  162 => 16,  156 => 35,  148 => 34,  140 => 33,  136 => 32,  127 => 11,  123 => 25,  119 => 24,  115 => 23,  111 => 30,  107 => 21,  102 => 18,  99 => 7,  97 => 26,  91 => 15,  88 => 14,  86 => 22,  83 => 12,  64 => 9,  57 => 8,  40 => 7,  37 => 6,  34 => 4,  28 => 4,);
    }
}
