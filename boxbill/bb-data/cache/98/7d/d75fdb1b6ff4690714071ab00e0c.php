<?php

/* partial_bb_meta.phtml */
class __TwigTemplate_987dd75fdb1b6ff4690714071ab00e0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <base href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["theme"]) ? $context["theme"] : null), "url"), "html", null, true);
        echo "\"/>
    <meta property=\"bb:url\" content=\"";
        // line 2
        echo twig_escape_filter($this->env, constant("BB_URL"), "html", null, true);
        echo "\"/>
    <meta property=\"bb:client_area\" content=\"";
        // line 3
        echo twig_escape_filter($this->env, twig_bb_client_link_filter("/"), "html", null, true);
        echo "\"/>
    ";
    }

    public function getTemplateName()
    {
        return "partial_bb_meta.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  17 => 1,  114 => 41,  109 => 4,  103 => 50,  97 => 47,  95 => 46,  89 => 42,  87 => 41,  71 => 33,  67 => 32,  59 => 29,  34 => 9,  32 => 8,  25 => 4,  20 => 1,  142 => 57,  133 => 51,  129 => 50,  119 => 49,  115 => 44,  105 => 39,  101 => 49,  93 => 34,  84 => 28,  74 => 23,  70 => 22,  60 => 17,  56 => 16,  48 => 21,  46 => 11,  41 => 9,  36 => 6,  33 => 5,  27 => 3,);
    }
}
