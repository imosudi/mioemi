<?php

/* mod_servicedomain_order.phtml */
class __TwigTemplate_836f356c4364bbfb240c5165485ba9dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'content_before' => array($this, 'block_content_before'),
            'content' => array($this, 'block_content'),
            'sidebar2' => array($this, 'block_sidebar2'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_meta_title($context, array $blocks = array())
    {
        echo gettext("Register new domain");
    }

    // line 7
    public function block_content_before($context, array $blocks = array())
    {
        // line 8
        $this->env->loadTemplate("partial_steps.phtml")->display(array_merge($context, array("selected" => 2)));
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "
";
        // line 13
        $context["periods"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "system_periods");
        // line 14
        $context["pricing"] = $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing");
        // line 15
        echo "
<form method=\"post\" action=\"\" class=\"api_form\" data-api-url=\"guest/cart/add_item\" data-api-redirect=\"";
        // line 16
        echo twig_escape_filter($this->env, twig_bb_client_link_filter("cart"), "html", null, true);
        echo "\">
    <div class=\"h-block\">
        <div class=\"h-block-header\">
            <div class=\"icon\"><span class=\"number\">2</span></div>
            <h2>";
        // line 20
        echo gettext("Configure");
        echo "</h2>
            <p>";
        // line 21
        echo gettext("The product/service you have chosen has the following configuration options for you to choose from.");
        echo "</p>
        </div>
        <div class=\"block conversation\">
    
            <div class=\"widget simpleTabs tabsRight first\">
                <div class=\"head\">
                    ";
        // line 28
        echo "                </div>

                <ul class=\"tabs\">
                    <li><a href=\"#register\" rel=\"register\"><span class=\"dark-icon\"></span>";
        // line 31
        echo gettext("Register new domain");
        echo "</a></li>
                    <li><a href=\"#transfer\" rel=\"transfer\"><span class=\"dark-icon\"></span>";
        // line 32
        echo gettext("Transfer my domain");
        echo "</a></li>
                </ul>

                <div class=\"tabs_container\" style=\"margin-top: 20px\">

                    <div class=\"tab_content\" id=\"register\">

                        <fieldset>
                            <legend>";
        // line 40
        echo gettext("Check domain availability");
        echo "</legend>
                            <p>
                                <input type=\"text\" name=\"register_sld\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "register_sld"), "html", null, true);
        echo "\" style=\"width: 583px\" placeholder=\"";
        echo gettext("Enter new domain name to register");
        echo "\">
                                ";
        // line 43
        echo $context["mf"]->getselectboxtld("register_tld", $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "serviceDomain_tlds", array(0 => array("allow_register" => 1)), "method"));
        echo "
                            </p>
                            <button class=\"bb-button bb-button-big\" id=\"domain-check\" style=\"float: right;\">";
        // line 45
        echo gettext("Check");
        echo "</button>
                            <button class=\"bb-button bb-button-submit bb-button-big order-button\" type=\"submit\" style=\"float: right; display: none;\">";
        // line 46
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "addons")) > 0)) {
            echo gettext("Continue");
        } else {
            echo gettext("Order now");
        }
        echo "</button>

                            <div class=\"onAfterDomainCheck\" style=\"display:none;\">
                                <label>
                                    <select name=\"register_years\" id=\"registration-years\"></select>
                                </label>
                                <br/>
                                <label><input type=\"checkbox\" onclick=\"\$('#nameservers').slideToggle();\">";
        // line 53
        echo gettext("I want to use my nameservers");
        echo "</label>
                                <div id=\"nameservers\" style=\"display:none; margin: 10px 0;\">
                                    <p><input type=\"text\" name=\"ns1\" value=\"\" placeholder=\"";
        // line 55
        echo gettext("Nameserver 1");
        echo "\"></p>
                                    <p><input type=\"text\" name=\"ns2\" value=\"\" placeholder=\"";
        // line 56
        echo gettext("Nameserver 2");
        echo "\"></p>
                                    <p><input type=\"text\" name=\"ns3\" value=\"\" placeholder=\"";
        // line 57
        echo gettext("Nameserver 3");
        echo "\"></p>
                                    <p><input type=\"text\" name=\"ns4\" value=\"\" placeholder=\"";
        // line 58
        echo gettext("Nameserver 4");
        echo "\"></p>
                                </div>

                            </div>

                        </fieldset>
                    </div>

                    <div class=\"tab_content\" id=\"transfer\">
                        <fieldset>
                            <p>
                                <input type=\"text\" name=\"transfer_sld\" value=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "transfer_sld"), "html", null, true);
        echo "\"  style=\"width: 583px\" placeholder=\"";
        echo gettext("Enter your domain name to transfer");
        echo "\">
                                ";
        // line 70
        echo $context["mf"]->getselectboxtld("transfer_tld", $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "serviceDomain_tlds", array(0 => array("allow_transfer" => 1)), "method"));
        echo "
                            </p>

                            <button class=\"bb-button bb-button-big\" type=\"button\" id=\"transfer-check\" style=\"float: right;\">";
        // line 73
        echo gettext("Check");
        echo "</button>
                            <button class=\"bb-button bb-button-submit bb-button-big order-button\" type=\"submit\" style=\"float: right; display: none;\">";
        // line 74
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "addons")) > 0)) {
            echo gettext("Continue");
        } else {
            echo gettext("Order now");
        }
        echo "</button>

                            <div id=\"domain-transfer-config\" style=\"display:none;\">
                                <p>";
        // line 77
        echo gettext("Transfer price");
        echo ": <span id=\"transfer-price\"></span></p>
                                <input type=\"text\" name=\"transfer_code\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "transfer_code"), "html", null, true);
        echo "\" style=\"width: 200px\" placeholder=\"";
        echo gettext("Enter domain transfer code");
        echo "\">
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class=\"clear\"></div>
            </div>

        </div>
        <div class=\"clear\"></div>
    </div>
    <div class=\"clear\"></div>
    ";
        // line 90
        $this->env->loadTemplate("partial_addons.phtml")->display(array_merge($context, array("product" => (isset($context["product"]) ? $context["product"] : null))));
        // line 91
        echo "    
<input type=\"hidden\" name=\"id\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
<input type=\"hidden\" name=\"action\" value=\"register\" id=\"domain-action\"/>
</form>
";
    }

    // line 97
    public function block_sidebar2($context, array $blocks = array())
    {
        // line 98
        echo "    ";
        $this->env->loadTemplate("partial_currency.phtml")->display($context);
    }

    // line 101
    public function block_js($context, array $blocks = array())
    {
        // line 102
        $context["currency"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "cart_get_currency");
        // line 103
        echo "<script type=\"text/javascript\">
\$(function() {

    \$('ul.tabs li a').bind('click', function (){
        \$('#domain-action').val(\$(this).attr('rel'));

        \$(this).parents('ul').find('span.dark-icon')
            .removeClass('i-check i-uncheck')
            .addClass('i-uncheck');

        \$(this).find('span')
            .removeClass('i-uncheck')
            .addClass('i-check');
    });
    \$('ul.tabs li a:first').click();

    if(\$(\".addons\").length) {
        \$('.order-button').one('click', function(){
            \$(this).hide();
            \$('.addons').slideDown('fast');
            return false;
        });
    }

    \$('.addon-period-selector').change(function(){
        var r = \$(this).attr('rel');
        \$('#' + r + ' span').hide();
        \$('#' + r + ' span.' + \$(this).val()).fadeIn();
    }).trigger('change');

    \$('#transfer-check').bind('click',function(event){
        var sld = \$('input[name=\"transfer_sld\"]').val();
        var tld = \$('select[name=\"transfer_tld\"]').val();
        var domain = sld + tld;
        bb.post(
            'guest/servicedomain/can_be_transferred',
            { sld: sld, tld: tld },
            function(result) {
                setTransferPricing(tld);
                \$('#domain-name').text(domain);
                \$('#domain-transfer-config').fadeIn('fast');
                \$('.onAfterDomainCheck').fadeIn('fast');
                \$('#transfer-check').hide();
                \$('#transfer .order-button').show();
            }
        );
        return false;
    });

    \$('#domain-check').bind('click',function(event){
        var sld = \$('input[name=\"register_sld\"]').val();
        var tld = \$('select[name=\"register_tld\"]').val();
        var domain = sld + tld;
        bb.post(
            'guest/servicedomain/check',
            { sld: sld, tld: tld },
            function(result) {
                setPricing(tld);
                \$('#domain-name').text(domain);
                \$('.onAfterDomainCheck').fadeIn('fast');
                \$('#domain-check').hide();
                \$('#register .order-button').show();
            }
        );
        return false;
    });

    function setPricing(tld)
    {
        bb.post(
            'guest/servicedomain/pricing',
            { tld: tld },
            function(result) {
                var s = \$(\"#registration-years\");
                s.find('option').remove();
                for (i=1;i<6;i++) {
                    var price = bb.currency(result.price_registration, ";
        // line 179
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currency"]) ? $context["currency"] : null), "conversion_rate"), "html", null, true);
        echo ", \"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currency"]) ? $context["currency"] : null), "code"), "html", null, true);
        echo "\", i);
                    s.append(new Option(i + \"";
        // line 180
        echo gettext(" Year/s @ ");
        echo "\" + price, i));
                }
            }
        );
    }

    function setTransferPricing(tld)
    {
        bb.post(
            'guest/servicedomain/pricing',
            { tld: tld },
            function(result) {
                var price = bb.currency(result.price_transfer, ";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currency"]) ? $context["currency"] : null), "conversion_rate"), "html", null, true);
        echo ", \"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currency"]) ? $context["currency"] : null), "code"), "html", null, true);
        echo "\");
                \$('#transfer-price').text(price);
            }
        );
    }
});
</script>
";
    }

    public function getTemplateName()
    {
        return "mod_servicedomain_order.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  333 => 192,  318 => 180,  312 => 179,  234 => 103,  232 => 102,  229 => 101,  224 => 98,  221 => 97,  213 => 92,  210 => 91,  208 => 90,  191 => 78,  187 => 77,  177 => 74,  173 => 73,  167 => 70,  161 => 69,  147 => 58,  143 => 57,  139 => 56,  135 => 55,  130 => 53,  116 => 46,  112 => 45,  107 => 43,  101 => 42,  96 => 40,  85 => 32,  81 => 31,  76 => 28,  67 => 21,  63 => 20,  56 => 16,  53 => 15,  51 => 14,  49 => 13,  46 => 12,  43 => 11,  39 => 8,  36 => 7,  30 => 5,  25 => 3,);
    }
}
