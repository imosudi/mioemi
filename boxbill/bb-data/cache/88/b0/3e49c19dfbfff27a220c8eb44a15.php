<?php

/* mod_client_settings.phtml */
class __TwigTemplate_88b03e49c19dfbfff27a220c8eb44a15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 4
        $context["active_menu"] = "system";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo "Client settings";
    }

    // line 6
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 7
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_bb_admin_link_filter("/"), "html", null, true);
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_bb_admin_link_filter("system"), "html", null, true);
        echo "\">";
        echo gettext("Settings");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 10
        echo gettext("Client settings");
        echo "</li>
</ul>
";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "
";
        // line 16
        $context["params"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "extension_config_get", array(0 => array("ext" => "mod_client")), "method");
        // line 17
        echo "<form method=\"post\" action=\"admin/extension/config_save\" class=\"mainForm api-form\" data-api-msg=\"";
        echo gettext("Configuration updated");
        echo "\">

<div class=\"widget simpleTabs\">

    <ul class=\"tabs\">
        <li><a href=\"#tab-index\">";
        // line 22
        echo gettext("General");
        echo "</a></li>
    </ul>
    
    <div class=\"tabs_container\">
        <div class=\"fix\"></div>
        <div id=\"tab-index\" class=\"tab_content nopadding\">

            <fieldset>
                <div class=\"rowElem noborder\">
                    <label>";
        // line 31
        echo gettext("Allow new signups");
        echo "</label>
                    <div class=\"formRight\">
                        <input type=\"radio\" name=\"allow_signup\" value=\"1\"";
        // line 33
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "allow_signup")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("Yes");
        echo "</label>
                        <input type=\"radio\" name=\"allow_signup\" value=\"0\"";
        // line 34
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "allow_signup"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>";
        echo gettext("No");
        echo "</label>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                <div class=\"rowElem\">
                    <label>";
        // line 39
        echo gettext("Require email confirmation");
        echo "</label>
                    <div class=\"formRight\">
                        <input type=\"radio\" name=\"require_email_confirmation\" value=\"1\"";
        // line 41
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "require_email_confirmation")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("Yes");
        echo "</label>
                        <input type=\"radio\" name=\"require_email_confirmation\" value=\"0\"";
        // line 42
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "require_email_confirmation"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>";
        echo gettext("No");
        echo "</label>
                    </div>
                    <div class=\"fix\"></div>
                </div>

                <div class=\"rowElem\">
                    <label>";
        // line 48
        echo gettext("Allow email change");
        echo "</label>
                    <div class=\"formRight\">
                        <input type=\"radio\" name=\"allow_change_email\" value=\"1\"";
        // line 50
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "allow_change_email")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("Yes");
        echo "</label>
                        <input type=\"radio\" name=\"allow_change_email\" value=\"0\"";
        // line 51
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "allow_change_email"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>";
        echo gettext("No");
        echo "</label>
                    </div>
                    <div class=\"fix\"></div>
                </div>

                <div class=\"rowElem\">
                    <label>";
        // line 57
        echo gettext("Required fields");
        echo "</label>
                    <div class=\"formRight\">
                        <select multiple=\"multiple\" class=\"multiple\" title=\"";
        // line 59
        echo gettext("Select required fields");
        echo "\" name=\"required[]\">
                            <optgroup label=\"General\">
                            <option value=\"last_name\" ";
        // line 61
        if (twig_in_filter("last_name", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Last Name");
        echo "</option>
                            <option value=\"company\" ";
        // line 62
        if (twig_in_filter("company", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Company");
        echo "</option>
                            <option value=\"gender\" ";
        // line 63
        if (twig_in_filter("gender", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Gender");
        echo "</option>
                            <option value=\"birthday\" ";
        // line 64
        if (twig_in_filter("birthday", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Birthday");
        echo "</option>
                            </optgroup>

                            <optgroup label=\"Address\">
                            <option value=\"country\" ";
        // line 68
        if (twig_in_filter("country", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Country");
        echo "</option>
                            <option value=\"city\" ";
        // line 69
        if (twig_in_filter("city", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("City");
        echo "</option>
                            <option value=\"state\" ";
        // line 70
        if (twig_in_filter("state", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("State");
        echo "</option>
                            <option value=\"address_1\" ";
        // line 71
        if (twig_in_filter("address_1", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Address Line 1");
        echo "</option>
                            <option value=\"address_2\" ";
        // line 72
        if (twig_in_filter("address_2", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Address Line 2");
        echo "</option>
                            <option value=\"postcode\" ";
        // line 73
        if (twig_in_filter("last_name", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Post code");
        echo "</option>
                            <option value=\"phone\" ";
        // line 74
        if (twig_in_filter("phone", $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "required"))) {
            echo "selected";
        }
        echo ">";
        echo gettext("Phone");
        echo "</option>
                            </optgroup>

                            ";
        // line 91
        echo "                        </select>

                    </div>
                    <div class=\"fix\"></div>
                </div>
                <input type=\"submit\" value=\"";
        // line 96
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
            </fieldset>
            
        </div>
    </div>
</div>
    
    <input type=\"hidden\" name=\"ext\" value=\"mod_client\" />
    </form>
";
    }

    public function getTemplateName()
    {
        return "mod_client_settings.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 96,  270 => 91,  260 => 74,  252 => 73,  244 => 72,  236 => 71,  228 => 70,  220 => 69,  212 => 68,  201 => 64,  193 => 63,  185 => 62,  177 => 61,  172 => 59,  167 => 57,  154 => 51,  146 => 50,  141 => 48,  128 => 42,  120 => 41,  115 => 39,  103 => 34,  95 => 33,  90 => 31,  78 => 22,  69 => 17,  67 => 16,  64 => 15,  61 => 14,  54 => 10,  48 => 9,  42 => 8,  39 => 7,  36 => 6,  30 => 3,  25 => 4,  23 => 2,);
    }
}
