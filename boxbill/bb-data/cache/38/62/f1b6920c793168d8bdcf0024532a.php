<?php

/* mod_servicedomain_tld.phtml */
class __TwigTemplate_3862f1b6920c793168d8bdcf0024532a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 4
        $context["active_menu"] = "system";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
    }

    // line 6
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 7
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_bb_admin_link_filter("/"), "html", null, true);
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_bb_admin_link_filter("servicedomain"), "html", null, true);
        echo "\">";
        echo gettext("Domain management");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
        echo "</li>
</ul>
";
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        // line 16
        echo "
<div class=\"widget\">

    <div class=\"head\">
        <h5>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
        echo " ";
        echo gettext("Top level domain management");
        echo "</h5>
    </div>

    <form method=\"post\" action=\"admin/servicedomain/tld_update\" class=\"mainForm save api-form\" data-api-msg=\"Top level domain settings updated\">
        <fieldset>
            <div class=\"rowElem noborder\">
                <label>";
        // line 26
        echo gettext("Registrar");
        echo ":</label>
                <div class=\"formRight\">
                    <select name=\"tld_registrar_id\">
                        ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicedomain_registrar_get_pairs"));
        foreach ($context['_seq'] as $context["id"] => $context["title"]) {
            // line 30
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
            echo "\" ";
            if (((isset($context["id"]) ? $context["id"] : null) == $this->getAttribute($this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "registrar"), "id"))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 32
        echo "                    </select>
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 38
        echo gettext("Registration price");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"price_registration\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "price_registration"), "html", null, true);
        echo "\" required=\"required\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 46
        echo gettext("Renewal price");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"price_renew\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "price_renew"), "html", null, true);
        echo "\" required=\"required\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 54
        echo gettext("Transfer price");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"price_transfer\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "price_transfer"), "html", null, true);
        echo "\" required=\"required\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 62
        echo gettext("Minimum years of registration");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"min_years\" value=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "min_years"), "html", null, true);
        echo "\" required=\"required\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 70
        echo gettext("Allow registration");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"radio\" name=\"allow_register\" value=\"1\"";
        // line 72
        if ($this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "allow_register")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
                    <input type=\"radio\" name=\"allow_register\" value=\"0\"";
        // line 73
        if ((!$this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "allow_register"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 79
        echo gettext("Allow transfer");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"radio\" name=\"allow_transfer\" value=\"1\"";
        // line 81
        if ($this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "allow_transfer")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
                    <input type=\"radio\" name=\"allow_transfer\" value=\"0\"";
        // line 82
        if ((!$this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "allow_transfer"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 88
        echo gettext("Active");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"radio\" name=\"active\" value=\"1\"";
        // line 90
        if ($this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "active")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
                    <input type=\"radio\" name=\"active\" value=\"0\"";
        // line 91
        if ((!$this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "active"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
                </div>
                <div class=\"fix\"></div>
            </div>


            <input type=\"hidden\" name=\"tld\" value=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
        echo "\" />
            <input type=\"submit\" value=\"";
        // line 98
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
        </fieldset>
    </form>
</div>

";
    }

    public function getTemplateName()
    {
        return "mod_servicedomain_tld.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 98,  236 => 97,  225 => 91,  219 => 90,  214 => 88,  203 => 82,  197 => 81,  192 => 79,  181 => 73,  175 => 72,  170 => 70,  161 => 64,  156 => 62,  147 => 56,  142 => 54,  133 => 48,  128 => 46,  119 => 40,  114 => 38,  106 => 32,  91 => 30,  87 => 29,  81 => 26,  70 => 20,  64 => 16,  61 => 15,  54 => 10,  48 => 9,  42 => 8,  39 => 7,  36 => 6,  30 => 3,  25 => 4,  23 => 2,);
    }
}
