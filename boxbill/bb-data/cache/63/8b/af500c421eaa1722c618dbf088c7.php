<?php

/* mod_servicehosting_server.phtml */
class __TwigTemplate_638baf500c421eaa1722c618dbf088c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_default.phtml");

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_default.phtml";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["active_menu"] = "system";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_meta_title($context, array $blocks = array())
    {
        echo "Hosting management";
    }

    // line 6
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 7
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_bb_admin_link_filter("/"), "html", null, true);
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_bb_admin_link_filter("servicehosting"), "html", null, true);
        echo "\">";
        echo gettext("Hosting plans and servers");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "name"), "html", null, true);
        echo "</li>
</ul>
";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "<div class=\"widget\">
    <div class=\"head\">
        <h5>Server management</h5>
    </div>

    <form method=\"post\" action=\"admin/servicehosting/server_update\" id=\"server-update\" class=\"mainForm save api-form\" data-api-msg=\"Server updated\">
        <fieldset>
            <div class=\"rowElem noborder\">
                <label>";
        // line 23
        echo gettext("Name");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"name\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "name"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Unique name to identify this server\">
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 30
        echo gettext("Hostname");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"hostname\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "hostname"), "html", null, true);
        echo "\" placeholder=\"server1.yourserverdomain.com\">
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 37
        echo gettext("IP");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"ip\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "ip"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Primary IP address of the server used to connect to it like: 123.123.123.123\">
                </div>
                <div class=\"fix\"></div>
            </div>
            ";
        // line 52
        echo "            <div class=\"rowElem\">
                <label>";
        // line 53
        echo gettext("Enable/Disable");
        echo ":</label>
                    <div class=\"formRight\">
                        <input type=\"radio\" name=\"active\" value=\"1\" ";
        // line 55
        if ($this->getAttribute((isset($context["server"]) ? $context["server"] : null), "active")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>Yes</label>
                        <input type=\"radio\" name=\"active\" value=\"0\" ";
        // line 56
        if ((!$this->getAttribute((isset($context["server"]) ? $context["server"] : null), "active"))) {
            echo "checked=\"checked\"";
        }
        echo "/><label>No</label>
                    </div>
                <div class=\"fix\"></div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Server manager</legend>
            <div class=\"rowElem\">
                <label>";
        // line 65
        echo gettext("Server manager");
        echo ":</label>
                <div class=\"formRight\">
                    <select name=\"manager\">
                        ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicehosting_manager_get_pairs"));
        foreach ($context['_seq'] as $context["code"] => $context["manager"]) {
            // line 69
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "\" ";
            if (($this->getAttribute((isset($context["server"]) ? $context["server"] : null), "manager") == (isset($context["code"]) ? $context["code"] : null))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["manager"]) ? $context["manager"] : null), "label"), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['code'], $context['manager'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 71
        echo "                    </select>
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 77
        echo gettext("Username");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"username\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "username"), "html", null, true);
        echo "\" placeholder=\"Login username to your server: root/reseller\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 85
        echo gettext("Password");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"password\" name=\"password\" value=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "password"), "html", null, true);
        echo "\" placeholder=\"Login password to your server\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 93
        echo gettext("Access Hash (Instead of password for cPanel servers)");
        echo ":</label>
                <div class=\"formRight\">
                    <textarea name=\"accesshash\" cols=\"5\" rows=\"5\">";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "accesshash"), "html", null, true);
        echo "</textarea>
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 101
        echo gettext("Connection port");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"port\" value=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "port"), "html", null, true);
        echo "\" placeholder=\"Custom port. Use blank to use default. Used to connect to API\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 109
        echo gettext("Use Secure connection");
        echo ":</label>
                    <div class=\"formRight\">
                        <input type=\"radio\" name=\"secure\" value=\"1\" ";
        // line 111
        if ($this->getAttribute((isset($context["server"]) ? $context["server"] : null), "secure")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>Yes</label>
                        <input type=\"radio\" name=\"secure\" value=\"0\" ";
        // line 112
        if ((!$this->getAttribute((isset($context["server"]) ? $context["server"] : null), "secure"))) {
            echo "checked=\"checked\"";
        }
        echo "/><label>No</label>
                    </div>
                <div class=\"fix\"></div>
            </div>

            <input type=\"button\" value=\"";
        // line 117
        echo gettext("Update and test connection");
        echo "\" class=\"greyishBtn submitForm\" id=\"test-connection\"/>
        </fieldset>


        <fieldset>
            <legend>Nameservers</legend>
            <div class=\"rowElem\">
                <label>";
        // line 124
        echo gettext("Nameserver 1");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"ns1\" value=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "ns1"), "html", null, true);
        echo "\" placeholder=\"ns1.yourdomain.com\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 132
        echo gettext("Nameserver 2");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"ns2\" value=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "ns2"), "html", null, true);
        echo "\" placeholder=\"ns2.yourdomain.com\">
                </div>
                <div class=\"fix\"></div>
            </div>

            <div class=\"rowElem\">
                <label>";
        // line 140
        echo gettext("Nameserver 3");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"ns3\" value=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "ns3"), "html", null, true);
        echo "\" placeholder=\"ns3.yourdomain.com\">
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 147
        echo gettext("Nameserver 4");
        echo ":</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"ns4\" value=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "ns4"), "html", null, true);
        echo "\" placeholder=\"ns4.yourdomain.com\">
                </div>
                <div class=\"fix\"></div>
            </div>
            <input type=\"submit\" value=\"";
        // line 153
        echo gettext("Update server");
        echo "\" class=\"greyishBtn submitForm\" />
        </fieldset>

        <input type=\"hidden\" name=\"id\" value=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "id"), "html", null, true);
        echo "\" />
    </form>
</div>

";
    }

    // line 162
    public function block_js($context, array $blocks = array())
    {
        // line 163
        echo "<script type=\"text/javascript\">
\$(function() {

    \$('#test-connection').click(function(){
        \$('#server-update').submit();
        api.post('admin/servicehosting/server_test_connection', {id:";
        // line 168
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "id"), "html", null, true);
        echo "}, function(result){
            bb.msg('Successfully connected to server');
        });
        return false;
    });

});
</script>

";
    }

    public function getTemplateName()
    {
        return "mod_servicehosting_server.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 168,  332 => 163,  329 => 162,  320 => 156,  314 => 153,  307 => 149,  302 => 147,  294 => 142,  289 => 140,  280 => 134,  275 => 132,  266 => 126,  261 => 124,  251 => 117,  241 => 112,  235 => 111,  230 => 109,  221 => 103,  216 => 101,  207 => 95,  202 => 93,  193 => 87,  188 => 85,  179 => 79,  174 => 77,  166 => 71,  151 => 69,  147 => 68,  141 => 65,  127 => 56,  121 => 55,  116 => 53,  113 => 52,  106 => 39,  101 => 37,  93 => 32,  88 => 30,  80 => 25,  75 => 23,  65 => 15,  62 => 14,  55 => 10,  49 => 9,  43 => 8,  40 => 7,  37 => 6,  31 => 2,  26 => 3,);
    }
}
