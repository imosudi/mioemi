<?php

/* mod_servicedomain_manage.phtml */
class __TwigTemplate_5246344c905588087c9ae7845e981d3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "status") == "active")) {
            // line 2
            echo "    <div class=\"widget simpleTabs tabsRight\">
        <div class=\"head\">
            <h2 class=\"dark-icon i-services\">";
            // line 4
            echo gettext("Domain management");
            echo "</h2>
        </div>

        <ul class=\"tabs\">
            <li><a href=\"#details\">";
            // line 8
            echo gettext("Details");
            echo "</a></li>
            <li><a href=\"#protection\">Protection</a></li>
            <li><a href=\"#privacy\">Privacy</a></li>
            <li><a href=\"#namservers\">Nameservers</a></li>
            <li><a href=\"#whois\">Whois</a></li>
            <li><a href=\"#epp\">Transfer</a></li>
        </ul>

        <div class=\"tabs_container\">

            <div class=\"tab_content box\" id=\"details\">
                <h2>";
            // line 19
            echo gettext("Domain details");
            echo "</h2>
                <table>
                    <tbody>
                        <tr>
                            <td>";
            // line 23
            echo gettext("Domain");
            echo "</td>
                            <td><a target=\"_blank\" href=\"http://";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "domain"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "domain"), "html", null, true);
            echo "</a></td>
                        </tr>
                        <tr>
                            <td>";
            // line 27
            echo gettext("Registration date");
            echo "</td>
                            <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_bb_date($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "registered_at")), "html", null, true);
            echo "</td>
                        </tr>

                        <tr>
                            <td>";
            // line 32
            echo gettext("Expires at");
            echo "</td>
                            <td>";
            // line 33
            echo twig_escape_filter($this->env, twig_bb_date($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "expires_at")), "html", null, true);
            echo "</td>
                        </tr>

                    </tbody>
                </table>
            </div>

            <div class=\"tab_content box\" id=\"epp\">
                <h2>";
            // line 41
            echo gettext("Domain Secret");
            echo "</h2>
                <div class=\"block\">
                    <p>";
            // line 43
            echo gettext("All domain names (except a .EU, .UK domain name) have a Domain (Transfer) Secret Key/Authorization Code (EPP Code) associated with them. This would be required if you want to transfer service.");
            echo "</p>
                    <p><button class=\"bb-button bb-button-submit\" type=\"button\" id=\"get-epp\">";
            // line 44
            echo gettext("Get EPP code");
            echo "</button></p>
                </div>
            </div>

            <div class=\"tab_content box\" id=\"protection\">
                <h2>";
            // line 49
            echo gettext("Domain Protection");
            echo "</h2>

                <div class=\"block\">
                <p>";
            // line 52
            echo gettext("Domain locking is a security enhancement to prevent unauthorized transfers of your domain to another registrar or web host by \"locking\" your domain name servers.");
            echo "</p>
                <p>
                    <button class=\"bb-button bb-button-submit\" type=\"button\" id=\"domain-unlock\" ";
            // line 54
            if (($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "locked") == 0)) {
                echo "style=\"display:none;\"";
            }
            echo ">";
            echo gettext("Unlock");
            echo "</button>
                    <button class=\"bb-button bb-button-submit\" type=\"button\" id=\"domain-lock\" ";
            // line 55
            if (($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "locked") == 1)) {
                echo "style=\"display:none;\"";
            }
            echo ">";
            echo gettext("Lock");
            echo "</button>
                </p>
                </div>
            </div>

            <div class=\"tab_content box\" id=\"privacy\">
                <h2>";
            // line 61
            echo gettext("Domain Privacy Settings");
            echo "</h2>
                <div class=\"block\">
                    <p>";
            // line 63
            echo gettext("If you would like to hide domain contact information which is shown on WHOIS you can enable privacy protection. Once domain privacy protection is enabled no one will know who registered this service. And once you decide to disable privacy protection, information that is listed above on \"Update Domain Contact Details\" section will be seen on domain WHOIS again.");
            echo "</p>
                    <p>
                        <button class=\"bb-button bb-button-submit\" type=\"button\" id=\"domain-disable-pp\" ";
            // line 65
            if (($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "privacy") == 1)) {
                echo "style=\"display:none;\"";
            }
            echo ">";
            echo gettext("Disable Privacy protection");
            echo "</button>
                        <button class=\"bb-button bb-button-submit\" type=\"button\" id=\"domain-enable-pp\" ";
            // line 66
            if (($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "privacy") == 0)) {
                echo "style=\"display:none;\"";
            }
            echo ">";
            echo gettext("Enable Privacy protection");
            echo "</button>
                    </p>
                </div>
            </div>

            <div class=\"tab_content box\" id=\"namservers\">
                <h2>";
            // line 72
            echo gettext("Domain Nameservers");
            echo "</h2>
                <div class=\"block\">
                <p>";
            // line 74
            echo gettext("If you would like to host this domain with another company you can update nameservers.");
            echo "</p>
                <form action=\"\" method=\"post\" id=\"update-nameservers\">
                    <fieldset>
                        <legend>";
            // line 77
            echo gettext("Update nameservers");
            echo "</legend>
                        <p>
                            <label>";
            // line 79
            echo gettext("Nameserver 1");
            echo ": </label>
                            <input type=\"text\" name=\"ns1\" value=\"";
            // line 80
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns1", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns1"), $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns1"))) : ($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns1"))), "html", null, true);
            echo "\" required=\"required\">
                        </p>

                        <p>
                            <label>";
            // line 84
            echo gettext("Nameserver 2");
            echo ": </label>
                            <input type=\"text\" name=\"ns2\" value=\"";
            // line 85
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns2", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns2"), $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns2"))) : ($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns2"))), "html", null, true);
            echo "\" required=\"required\">
                        </p>

                        <p>
                            <label>";
            // line 89
            echo gettext("Nameserver 3");
            echo ": </label>
                            <input type=\"text\" name=\"ns3\" value=\"";
            // line 90
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns3", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns3"), $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns3"))) : ($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns3"))), "html", null, true);
            echo "\">
                        </p>

                        <p>
                            <label>";
            // line 94
            echo gettext("Nameserver 4");
            echo ": </label>
                            <input type=\"text\" name=\"ns4\" value=\"";
            // line 95
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns4", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ns4"), $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns4"))) : ($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "ns4"))), "html", null, true);
            echo "\">
                        </p>
                        <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo "\">
                        <input class=\"bb-button bb-button-submit\" type=\"submit\" value=\"";
            // line 98
            echo gettext("Update");
            echo "\">
                    </fieldset>
                </form>

                </div>
            </div>

            <div class=\"tab_content box\" id=\"whois\">
                <h2>";
            // line 106
            echo gettext("Domain Contact Details");
            echo "</h2>
                <div class=\"block\">
                    <p>";
            // line 108
            echo gettext("Domain contact details will be displayed once someone will check WHOIS output (which is public) of your service. This will update Technical, Billing and Admin contacts for this service. You can enable domain privacy protection if you want to hide your public WHOIS information.");
            echo "</p>
                    <form method=\"post\" action=\"\" id=\"update-contact\">
                        <fieldset>
                            <legend>";
            // line 111
            echo gettext("Update domain contact details");
            echo "</legend>
                            <p>
                                <label>";
            // line 113
            echo gettext("First Name");
            echo ": </label>
                                <input type=\"text\" name=\"contact[first_name]\" value=\"";
            // line 114
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "first_name", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "first_name"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "first_name"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "first_name"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 118
            echo gettext("Last Name");
            echo ": </label>
                                <input type=\"text\" name=\"contact[last_name]\" value=\"";
            // line 119
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "last_name", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "last_name"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "last_name"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "last_name"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 123
            echo gettext("Email");
            echo ": </label>
                                <input type=\"text\" name=\"contact[email]\" value=\"";
            // line 124
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "email", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "email"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "email"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "email"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 128
            echo gettext("Company");
            echo ": </label>
                                <input type=\"text\" name=\"contact[company]\" value=\"";
            // line 129
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "company", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "company"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "company"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "company"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 133
            echo gettext("Address Line 1");
            echo ": </label>
                                <input type=\"text\" name=\"contact[address1]\" value=\"";
            // line 134
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "address1", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "address1"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "address1"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "address1"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 138
            echo gettext("Address Line 2");
            echo ": </label>
                                <input type=\"text\" name=\"contact[address2]\" value=\"";
            // line 139
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "address2", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "address2"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "address2"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "address2"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 143
            echo gettext("Country");
            echo ": </label>
                                <input type=\"text\" name=\"contact[country]\" value=\"";
            // line 144
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "country", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "country"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "country"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "country"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 148
            echo gettext("City");
            echo ": </label>
                                <input type=\"text\" name=\"contact[city]\" value=\"";
            // line 149
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "city", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "city"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "city"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "city"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 153
            echo gettext("State");
            echo ": </label>
                                <input type=\"text\" name=\"contact[state]\" value=\"";
            // line 154
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "state", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "state"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "state"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "state"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 158
            echo gettext("Zip");
            echo ": </label>
                                <input type=\"text\" name=\"contact[postcode]\" value=\"";
            // line 159
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "postcode", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "postcode"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "postcode"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "postcode"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 163
            echo gettext("Phone Country Code");
            echo ": </label>
                                <input type=\"text\" name=\"contact[phone_cc]\" value=\"";
            // line 164
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "phone_cc", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "phone_cc"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "phone_cc"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "phone_cc"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <p>
                                <label>";
            // line 168
            echo gettext("Phone number");
            echo ": </label>
                                <input type=\"text\" name=\"contact[phone]\" value=\"";
            // line 169
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "phone", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "phone"), $this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "phone"))) : ($this->getAttribute($this->getAttribute((isset($context["service"]) ? $context["service"] : null), "contact"), "phone"))), "html", null, true);
            echo "\" required=\"required\">
                            </p>

                            <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo "\">
                            <input class=\"bb-button bb-button-submit\" type=\"submit\" value=\"";
            // line 173
            echo gettext("Update");
            echo "\">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
</div>

<script type=\"text/javascript\">
\$(function() {

    \$('#domain-lock').bind('click',function(event){
        bb.post(
            'client/servicedomain/lock',
            { order_id: ";
            // line 187
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo " },
            function(result) {
                bb.msg('Domain locked');
                \$('#domain-lock').toggle();
                \$('#domain-unlock').toggle();
            }
        );
        return false;
    });

    \$('#domain-unlock').bind('click',function(event){
        bb.post(
            'client/servicedomain/unlock',
            { order_id: ";
            // line 200
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo " },
            function(result) {
                bb.msg('Domain unlocked');
                \$('#domain-lock').toggle();
                \$('#domain-unlock').toggle();
            }
        );
        return false;
    });

    \$('#domain-enable-pp').bind('click',function(event){
        bb.post(
            'client/servicedomain/enable_privacy_protection',
            { order_id: ";
            // line 213
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo " },
            function(result) {
                bb.msg('Privacy Protection enabled');
                \$('#domain-enable-pp').toggle();
                \$('#domain-disable-pp').toggle();
            }
        );
        return false;
    });

    \$('#domain-disable-pp').bind('click',function(event){
        bb.post(
            'client/servicedomain/disable_privacy_protection',
            { order_id: ";
            // line 226
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo " },
            function(result) {
                bb.msg('Privacy Protection disabled');
                \$('#domain-enable-pp').toggle();
                \$('#domain-disable-pp').toggle();
            }
        );
        return false;
    });

    \$('#get-epp').bind('click',function(event){
        bb.post(
            'client/servicedomain/get_transfer_code',
            { order_id: ";
            // line 239
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo " },
            function(result) {
                bb.msg('Domain transfer code is: ' + result);
            }
        );
        return false;
    });

    \$('#update-nameservers').bind('submit',function(event){
        bb.post(
            'client/servicedomain/update_nameservers',
            \$(this).serialize(),
            function(result) {
                bb.msg('Nameservers updated');
            }
        );
        return false;
    });

    \$('#update-contact').bind('submit',function(event){
        bb.post(
            'client/servicedomain/update_contacts',
            \$(this).serialize(),
            function(result) {
                bb.msg('Contact details updated');
            }
        );
        return false;
    });

});
</script>

";
        }
    }

    public function getTemplateName()
    {
        return "mod_servicedomain_manage.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  474 => 239,  458 => 226,  442 => 213,  426 => 200,  410 => 187,  393 => 173,  389 => 172,  383 => 169,  379 => 168,  372 => 164,  368 => 163,  361 => 159,  357 => 158,  350 => 154,  346 => 153,  339 => 149,  335 => 148,  328 => 144,  324 => 143,  317 => 139,  313 => 138,  306 => 134,  302 => 133,  295 => 129,  291 => 128,  284 => 124,  280 => 123,  269 => 118,  258 => 113,  253 => 111,  247 => 108,  242 => 106,  231 => 98,  227 => 97,  222 => 95,  218 => 94,  207 => 89,  200 => 85,  196 => 84,  189 => 80,  185 => 79,  174 => 74,  169 => 72,  156 => 66,  148 => 65,  143 => 63,  138 => 61,  125 => 55,  117 => 54,  112 => 52,  106 => 49,  98 => 44,  89 => 41,  78 => 33,  74 => 32,  67 => 28,  63 => 27,  55 => 24,  44 => 19,  30 => 8,  19 => 2,  17 => 1,  619 => 296,  614 => 294,  605 => 287,  602 => 285,  597 => 255,  594 => 253,  589 => 251,  586 => 250,  584 => 249,  576 => 246,  573 => 245,  566 => 240,  555 => 237,  551 => 236,  543 => 235,  539 => 234,  535 => 233,  531 => 232,  526 => 231,  522 => 230,  513 => 224,  509 => 223,  505 => 222,  501 => 221,  497 => 220,  489 => 215,  485 => 213,  483 => 212,  481 => 211,  479 => 210,  469 => 203,  464 => 201,  455 => 197,  451 => 196,  444 => 192,  440 => 191,  431 => 187,  427 => 186,  420 => 182,  416 => 181,  406 => 174,  395 => 166,  391 => 165,  384 => 161,  380 => 160,  373 => 156,  369 => 155,  362 => 151,  358 => 150,  353 => 148,  345 => 143,  334 => 135,  329 => 133,  322 => 129,  318 => 128,  309 => 124,  305 => 123,  298 => 119,  294 => 118,  285 => 112,  273 => 119,  267 => 100,  265 => 99,  262 => 114,  252 => 96,  250 => 95,  246 => 94,  243 => 93,  237 => 91,  235 => 90,  232 => 89,  226 => 87,  224 => 86,  216 => 80,  211 => 90,  209 => 76,  203 => 74,  198 => 72,  195 => 71,  193 => 70,  190 => 69,  184 => 66,  180 => 77,  177 => 64,  175 => 63,  172 => 62,  162 => 59,  152 => 58,  149 => 57,  147 => 56,  137 => 53,  133 => 52,  126 => 48,  122 => 47,  115 => 43,  111 => 42,  107 => 40,  101 => 37,  97 => 36,  94 => 43,  92 => 34,  86 => 31,  82 => 30,  75 => 26,  71 => 25,  64 => 21,  60 => 20,  51 => 23,  47 => 13,  41 => 9,  38 => 8,  32 => 3,  27 => 6,  25 => 5,  23 => 4,);
    }
}
