<?php

/* partial_addons.phtml */
class __TwigTemplate_ebab47774e7db3fe1a57f68e39a58ee6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "addons")) > 0)) {
            // line 2
            echo "<div class=\"addons\" style=\"display: none;\">
<div class=\"widget\">
    <div class=\"head\">
        <h2 class=\"dark-icon i-services\">";
            // line 5
            echo gettext("Addons & extras");
            echo "</h2>
    </div>
    <table>
        <tbody>
            ";
            // line 9
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "addons"));
            foreach ($context['_seq'] as $context["_key"] => $context["addon"]) {
                // line 10
                echo "            <tr>
                <td>
                    <input type=\"hidden\" name=\"addons[";
                // line 12
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "][selected]\" value=\"0\">
                    <input type=\"checkbox\" name=\"addons[";
                // line 13
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "][selected]\" value=\"1\" id=\"addon_";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\">
                </td>
                <td>
                    <label for=\"addon_";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "icon_url"), "html", null, true);
                echo "\" alt=\"\" width=\"36\"/></label>
                </td>
                <td style=\"width: 50%\">
                    <label for=\"addon_";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\"><h2>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "title"), "html", null, true);
                echo "</h2></label>
                    ";
                // line 20
                echo twig_bbmd_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "description"));
                echo "
                </td>
                <td>
                    ";
                // line 23
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "recurrent")) {
                    // line 24
                    echo "                    <select name=\"addons[";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                    echo "][period]\" class=\"addon-period-selector\" rel=\"addon-periods-";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                    echo "\">
                    ";
                    // line 25
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "recurrent"));
                    foreach ($context['_seq'] as $context["code"] => $context["prices"]) {
                        // line 26
                        echo "                        ";
                        if ($this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "enabled")) {
                            // line 27
                            echo "                        <option value=\"";
                            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["periods"]) ? $context["periods"] : null), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "html", null, true);
                            echo "</option>
                        ";
                        }
                        // line 29
                        echo "                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['code'], $context['prices'], $context['_parent'], $context['loop']);
                    $context = array_merge($_parent, array_intersect_key($context, $_parent));
                    // line 30
                    echo "                    </select>
                    ";
                }
                // line 32
                echo "                </td>
                <td class=\"currency\" style=\"width: 20%\">
                    ";
                // line 34
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "recurrent")) {
                    // line 35
                    echo "                    <div id=\"addon-periods-";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                    echo "\">
                    ";
                    // line 36
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "recurrent"));
                    foreach ($context['_seq'] as $context["code"] => $context["prices"]) {
                        // line 37
                        echo "                        <span class=\"";
                        echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
                        echo "\">";
                        echo twig_money_convert($this->env, ($this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "price") + $this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "setup")));
                        echo "</span>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['code'], $context['prices'], $context['_parent'], $context['loop']);
                    $context = array_merge($_parent, array_intersect_key($context, $_parent));
                    // line 39
                    echo "                    </div>
                    ";
                }
                // line 41
                echo "
                    ";
                // line 42
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "once")) {
                    // line 43
                    echo "                        ";
                    echo twig_money_convert($this->env, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "once"), "price") + $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "once"), "setup")));
                    echo "
                    ";
                }
                // line 45
                echo "
                    ";
                // line 46
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "free")) {
                    // line 47
                    echo "                        ";
                    echo twig_money_convert($this->env, 0);
                    echo "
                    ";
                }
                // line 49
                echo "                </td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['addon'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 52
            echo "        </tbody>
    </table>
</div>
<div class=\"block\" style=\"text-align: right; margin-bottom:10px;\">
    <button class=\"bb-button bb-button-submit bb-button-big\" type=\"submit\">";
            // line 56
            echo gettext("Order now");
            echo "</button>
</div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "partial_addons.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 56,  165 => 52,  157 => 49,  151 => 47,  149 => 46,  146 => 45,  140 => 43,  138 => 42,  131 => 39,  120 => 37,  111 => 35,  109 => 34,  105 => 32,  95 => 29,  87 => 27,  84 => 26,  80 => 25,  73 => 24,  71 => 23,  65 => 20,  59 => 19,  35 => 10,  31 => 9,  24 => 5,  19 => 2,  17 => 1,  333 => 192,  318 => 180,  312 => 179,  234 => 103,  232 => 102,  229 => 101,  224 => 98,  221 => 97,  213 => 92,  210 => 91,  208 => 90,  191 => 78,  187 => 77,  177 => 74,  173 => 73,  167 => 70,  161 => 69,  147 => 58,  143 => 57,  139 => 56,  135 => 41,  130 => 53,  116 => 36,  112 => 45,  107 => 43,  101 => 30,  96 => 40,  85 => 32,  81 => 31,  76 => 28,  67 => 21,  63 => 20,  56 => 16,  53 => 15,  51 => 16,  49 => 13,  46 => 12,  43 => 13,  39 => 12,  36 => 7,  30 => 5,  25 => 3,);
    }
}
