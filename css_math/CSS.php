<?php
/** CSS loading and interpretation.
 *
 * @copyright  2015 Chris Sprucefield
 * @author     Chris Sprucefield <csprucefield@gmail.com>
 * @version    1.0.0
 * @license    CC Attribution-ShareAlike 4.0 International
 * @filesource
 */

class CSS
{
	private static $conf ; 
	
	/** Include a dynamic CSS file.
	 * A conf variable can be accessed by placing the name within brackets, in uppercase in the CSS file,
	 * such as: $conf['_theme_img'] --> [_theme_img]
	 *
	 * @method string load(string $name)
	 * @global mixed $conf Global config variable.
	 * @uses       self::cssmath
	 * @param      string $name The name of the CSS file to load.
	 * @return     string The returned CSS code.
	 */

	public static function setConf($conf)
	{
		self::$conf = $conf ; 
	}
	
	/** Include a dynamic CSS file.
	 * A conf variable can be accessed by placing the name within brackets, in uppercase in the CSS file,
	 * such as: $conf['_theme_img'] --> [_theme_img]
	 *
	 * @method string load(string $name)
	 * @global mixed $conf Global config variable.
	 * @uses       self::cssmath
	 * @param      string $name The name of the CSS file to load.
	 * @return     string The returned CSS code.
	 */
	public static function load($name)
	{
		if (file_exists("{$name}.css"))
		{
			$css = file_get_contents("{$name}.css");

			// Only use the bracket keys in the file.
			preg_match_all("/\[[^\]]*\]/",$css,$out) ;
			if (is_array($out[0]))
			{
				foreach ($out[0] as $id => $key)
				{
					$keyname = str_replace(array("[", "]"), "", strtoupper($key));

					// Basic math present?
					list($keyname,$math) = self::cssmath($keyname) ;

					// Items is the preferred one, as some calculations are done.
					$floor = 0 ;
					if ($keyname[0] == '~')
					{
						$keyname = substr($keyname, 1) ;
						$floor = 1 ;
					}
					$val = self::$conf[$keyname] ;

					if ($math != "")
					{
						$calc = str_replace($keyname,$val,$math) ;
						$calc = str_replace(" ","",$calc) ;
						$val = eval("return({$calc});") ;
						$val = $floor == 1 ? floor($val) : $val ;
						$css = str_replace("[{$math}]", $val, $css);
					} else
					{
						$css = str_replace("[{$keyname}]", $val, $css);
					}
				}
			}
			return("<style>{$css}</style>") ;
		}
	}

	/** Allow simple math in the CSS field.
	 * In the case a config field is a simple number, allow the form FIELD_NAME[+ - * /]val[...]
	 *
	 * @method string cssmath(string $keyname)
	 * @param      string $keyname The config value key name.
	 * @return     array array($key,$math) where math is the math expression to evaluate.
	 */
	private static function cssmath($keyname)
	{
		// Basic math operators (including space for good measure)
		$ops = array(" ","+","-","*","/") ;
		$conv = str_replace($ops, $ops[0], $keyname);
		$conv = str_replace(array("(",")"),"",$conv) ;
		list($key,$math) = explode($ops[0], $conv,2) ;

		$math = $math != "" ? $keyname : "" ;

		return(array($key,$math)) ;
	}
}
