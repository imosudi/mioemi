/*******************
  Copyright (c) 2004-2015 TinyWebGallery
  written by Michael Dempfle
  TWG version: 2.3.4
********************/

Readme - TinyWebgallery v2.3.4

Please go to the web site (Installation) for the full English documentation!
www.tinywebgallery.com

What you need
-------------
php >= 4.3.0, gdlib > 2.0 and some nice pictures.
Optional: Flash 8 Plugin to use the TWG Upload Flash.

Installation with Installer 
--------------------------
You have download the installer version if TWG. It has the following advantages:
 - You only have to upload 3 files to the webspace (much faster then the normal way).
 - A pre installation check of the system is done before the installation!
 - All temporary folders are created with the needed rights.
 - You can update TWG if you have stored your changes in my_config.php and my_style.css.

1. Extract the archive to a directory (I think you did this already somehow :))
2. Copy the 3 files starting with install* to your web server into the directory where you want to install TWG.
3. Execute install.php and follow the installer.
   To use the Upload Flash you maybe have to copy a .htaccess file to the directory of the
	 upload flash (admin/upload). The file is stored in a zip in this directory. Simply try
	 the Upload Flash (Use Upload in the TWGXplorer). It will tell you if you need to extract 
   the .htaccess file. Please go to the TWG Flash Uploader section of the  website for more
   support.
   
-  If you want to support TWG please give feedback, click on some ad's on the website or
   vote for TWG (links are on the website).
-  If everything is running please go to the website and read the how-to's. 
   There are many possibilities, to use TWG better and more effective. 
-  If you want to remove the "powered by TWG" or using this gallery for commercial purposes please
   register. Thank you

If the installer does NOT work you can make a local installation if you have a web server. 
You can also extract the install_twg.zip and move the files from the 'full' directory to the main 
directory. Then you upload the directory like described in readme_en.txt.
To improve the installer lease send me some feedback if the installer fails.

Configuration
-------------
See readme_en.txt

Update with the Installer
-------------------------
You can use the installer to update a TWG 1.4 installation. Older version are not supported!
You can do is like a normal installation described above. 

PLEASE NOTE: The update is only tested if you have installed TWG with the installer before! 
Otherwise I recommend to update TWG by overwritting the files from the normal zip. Depending 
on your system there can be problems because of the different users you are using when you have 
installed your 1st version of TWG by FTP and now try to use the 2nd with the installer! 

TWG is detecting an existing version and is updating all files except the my_config.php,
my_style.css and the user file admin/_config/.htusers.php. And the demo gallery is not 
installed anymore! If you have changed code or images of TWG please use the normal version
of TWG and move your changes to the new version!
If you have made changes in the config.php -> store them in the my_config.php. If you have
done the configuration with the TWG Admin your config is already stored in my_config.php.
If you want to use the config you should save the additional parameters and enter them again
in the new front-end of TWG Admin.
If you update the config.php is backuped just in case! You can delete the backup 
after an successful update. Please make a backup before updating - just in case ;).
(I recommend the xml, the counter folder and the config files!)

Registration - Removing "powered by TWG"
----------------------------------------
See readme_en.txt

Extended Support
------------------
See readme_en.txt 

Administration
--------------
See readme_en.txt

Have fun,

Michael Dempfle (tinywebgallery ( a t ) mdempfle . de)

www.tinywebgallery.com
