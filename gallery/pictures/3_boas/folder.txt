<p>This folder shows the support of embedded mp3's using the streaming flash player from TWG. Please read "howto 34" regarding setup and pros and cons of this solution.</p>
<p>The music on this demo page is from the alternative rock band <a href="http://www.brain-on-a-stick.de">brain-on-a-stick</a> from Munich (Germany). The featured songs are part of their current album "Strange Proposals". </p>
<p/>
<a href="http://itunes.apple.com/de/artist/brain-on-a-stick/id379358850" >&raquo; brain-on-a-stick at iTunes</a><br />
<a href="http://www.amazon.de/gp/product/brain-on-a-stick/B003TVC9H4">&raquo; brain-on-a-stick at Amazon</a>