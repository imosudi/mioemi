<p>Dieses Beispiel zeigt die Unterstützung vom embedded MP3's mit dem Streaming-Flash-Player der TWG. Bitte lest howto 34 um zu erfahren,  wie man dies aufsetzt und welche Vor- und Nachteile diese Lösung bietet.</p>
<p>Die Songs auf dieser Seite sind von dem aktuellen Album "Strange Proposals" der Alternative Rock Band <a href="http://www.brain-on-a-stick.de">brain-on-a-stick</a> aus München.</p>
<p/>
<a href="http://itunes.apple.com/de/artist/brain-on-a-stick/id379358850" >&raquo; brain-on-a-stick bei iTunes</a><br />
<a href="http://www.amazon.de/gp/product/brain-on-a-stick/B003TVC9H4">&raquo; brain-on-a-stick bei Amazon</a>