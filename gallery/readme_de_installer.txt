/*******************
  Copyright (c) 2004-2015 TinyWebGallery
  written by Michael Dempfle
  TWG version: 2.3.4
********************/

Readme - TinyWebgallery v2.3.4

Weitere Informationen findet ihr auf der Webseite (Installation) 
www.tinywebgallery.com

Systemvoraussetzungen
---------------------
Ihr braucht Webspache mit php >= 4.3.0 und gdlib > 2.0
Optional: Flash 8 Plugin für den TWG Upload Flash.

Installation mit Installer 
--------------------------
Ihr habt die Version mit Installer heruntergeladen. Diese hat den Vorteil
 - das man nur 3 Dateien auf den Webspace kopieren muss (Ist deutlich schneller als alles einzeln)
 - Vor der Installation ein Test des Systems stattfindet!
 - Alle temporären Verzeichnisse mit den entsprechenden Rechten erzeugt werden.
 - Man TWG updaten kann, wenn man seine Änderungen nur in der my_config.php und my_style.css gemacht hat

1. Auspacken des Archivs (habt ihr ja schon geschafft)
2. Kopiert die 3 Dateien die mit install* anfangen auf den Webserver
3. Führt dort die install.php aus und folgt dem Installer. 
   Beim Upload-Flash muss evtl. noch eine .htaccess Datei in das Verzeichnis des TWG Upload
   Flash enpackt werden. Dies befindet sich im Ordner admin/upload. Am einfachsten
   probiert man den Upload Flash einfach mal aus (Upload im TWGXplorer). Wenn die Datei benötigt 
   wird, gibt dieser eine Warnung aus! Bitte geht zum TWG Flash Uploaderbereich auf der Webseite
   für weitere Infos.

-  Wenn ihr die Galerie unterstützen wollte, gebt mir doch etwas Feedback, klickt auf
   die Werbung auf der Webseite oder bewertet TWG (Links sind auf der Webseite vorhanden).
-  Wenn mal alles läuft, geht bitte auf die Webseite und schaut euch die how-to's an. Es
   gibt jede Menge Möglichkeiten, die Galerie noch effektiver und besser zu nutzen. 
-  Wenn ihr das "powered by TWG" entfernen wollt oder die Galerie nicht zu privaten Zwecken nützt
   könnt ihr euch auf der webseite registrieren.

Wenn der Installer NICHT funktioniert könnt ihr den Installer lokal ausführen (Wenn ihr einen
lokalen Webserver habt) und das Verzeichnis wie die normale Version hochladen (Siehe readme_de.txt). 
Wenn nicht, könnt ihr das Zipfile auch einfach entpacken und alles aus dem 'full' Verzeichnis ins
Hauptverzeichnis verschieben. Danach könnt ihr TWG wie im normalen readme beschrieben installieren.
Es währe auch nett, dann Feedback zu bekommen, warum der Installer nicht funktioniert hat, damit
ich diesen Verbessern kann.

Konfiguration
-------------
siehe readme_de.txt

Update mit Installer
--------------------
Ihr könnt den Installer auch zum Updaten einer 1.4 Installation verwenden (Ältere Versionen
werden nicht unterstützt!). Geht genau, wie oben beschrieben vor.

BITTE BEACHTEN: Das Update ist nur getestet, wenn TWG zuvor mit dem Installer installiert worden ist.
Sonst empfehle ich einen update durch das überschreiben der Dateien aus der Zip-Datei.
Abhängig von eurem System kann es nämlich passieren, das hier Probleme auftreten, weil der 
Installer ein anderer Benutzer ist, wei der FTP Benutzer.

TWG entdeckt eine bestehende Version und aktualisiert alle Dateien außer der my_config.php,
my_style.css und der Benutzerdatei admin/_config/.htusers.php. Außerdem wird die Beispiel-
bildergalerie nicht mehr installiert! Wenn ihr also z.B. eigene Buttons verwendet, nehmt bitte
die normale Version und baut dort eure Änderungen wieder ein!
Wenn ihr Anpassungen in der config.php gemacht habt (wenn Ihr die config über den TWG admin geändert 
habt, ist bereits alles in der my_config.php gespeichert), verschiebt diese in die my_config.php.
Falls ihr den TWG admin für die config verwenden wollt, sichert die zusätzlichen Einstellungen
und pflegt diese über das Frontent ein!
Bitte macht vor jedem Update immer ein Backup! Wenn ihr sicher gehen wollt, sichert den xml und 
den counter Ordner und die Konfigurationsdateien.

Registrierung = Entfernen des "powered by TWG"
----------------------------------------------
siehe readme_de.txt
 
Erweiterter Support
------------------
siehe readme_de.txt

Administration
--------------
siehe readme_de.txt

Viel Spaß,

Michael Dempfle (tinywebgallery ( a t ) mdempfle . de)

www.tinywebgallery.com
